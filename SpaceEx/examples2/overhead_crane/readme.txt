This example is a simple, classic LTI model of an overhead crane. 

The coefficients were taken from 
   Mathias Kluwe. Regelung linearer Mehrgroessensysteme, 2010. Lecture notes.

The state variables are:
- x1 : horizontal crane position
- x2 : horizontal velocity
- x3 : angle of the hook with respect to the vertical axis
- x4 : angular velocity of the hook
