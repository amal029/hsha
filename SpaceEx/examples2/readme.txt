This folder contains a collection of examples for the SpaceEx platform. 

Each example consists of a model file (.xml) and one or several configuration files (.cfg). If several configuration files are present, they represent either different system configurations (initial and forbidden states), or analysis parameters (algorithm, time step, output).

Further details are given in the readme file in each subfolder.
