This example is a simple, classic LTI model of three tanks. 

The coefficients were taken from 
   Mathias Kluwe. Regelung linearer Mehrgroessensysteme, 2010. Lecture notes.

The three tanks are set up in a row, and adjacent tanks are connected.
The state variables are:
- x : fill height first tank
- y : fill height second tank
- z : fill height third tank
