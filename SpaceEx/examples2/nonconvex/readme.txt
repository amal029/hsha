This example demonstrates the ability of the STC scenario to detect nonconvex flowpipes.

The system has three variables x,y,z. Variable x does not change at all, variable y 
changes at a constant rate given by the initial value of x and he variable z is a clock. 
The reach set of the example is a straight line at every point in time. The projection 
of the reachable set from z=0 to z=1 on variables x and y is a bowtie.
Here, the nonconvexity of the flowpipe does not come from complex dynamics, which makes
it difficult to detect. The STC scenario correctly reproduces a bowtie up to the given
flowpipe tolerance. The LGG scenario completely fails to adapt time steps
accordingly, since its time step adaptation is based purely on the dynamics, which are
trivial in this example.
