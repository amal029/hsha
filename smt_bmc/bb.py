#!/usr/bin/env python3

from z3 import And, Real, Bool, sat
from z3 import Or, Not, SolverFor


def main(TIMEOUT=60, k=1000):
    d = Real('d')
    s = SolverFor('QF_NRA')
    # Settimeout
    s.set('timeout', TIMEOUT)
    s.add(d > 0)
    s.add(d < 1)

    # height and velocity
    h = [Real('h_%s' % i) for i in range(k+1)]
    v = [Real('v_%s' % i) for i in range(k+1)]

    # State encoder
    a = [Bool('a_%s' % i) for i in range(k+1)]

    # Tunable variable
    t = [Real('t_%s' % i) for i in range(k+1)]

    # Initial conditions
    s.add(And(Not(a[0]), h[0] == 100, v[0] == 10, t[0] == 0))

    g = -9.81
    # Now the state transitions
    for i in range(1, k+1):
        s.add(Or(
            And(Not(a[i-1]), Not(a[i]), h[i-1] > 0,
                v[i] == v[i-1] + d * g,
                h[i] == h[i-1] + d * v[i-1],
                t[i] == t[i-1] + d),
            And(Not(a[i-1]), Not(a[i]), h[i-1] == 0,
                h[i] == h[i-1] + 0.00001,
                v[i] == v[i-1]*(-0.5),
                t[i] == t[i-1])))

    # A property to force it to move
    s.add(Or(*[h[i] == 0 for i in range(k+1)]))
    # Safety property
    s.add(And(*[h[i] >= 0 for i in range(k+1)]))
    print(s.sexpr())
    ret = s.check()
    if ret == sat:
        # print(s.model())
        print(s.model()[d])
        for i in range(k+1):
            print(s.model()[t[i]].as_decimal(9),
                  ',', s.model()[h[i]].as_decimal(9),
                  ',', s.model()[v[i]].as_decimal(9))
    print(ret)
    return ret


if __name__ == '__main__':
    main(TIMEOUT=100000, k=10)
