#!/usr/bin/env python3

from z3 import And, Real, Bool, sat
import timeit
from z3 import Or, Not, SolverFor, Implies


def t1_t1_ts(a, b, K, h, x, k, d, ON, OFF):
    return Or(And(Not(a[k-1]), Not(b[k-1]), Not(a[k]), Not(b[k]),
                  Not(ON[k-1]), Not(OFF[k-1]),
                  x[k-1] == 20, x[k] == x[k-1] + d * 0),
              And(Not(a[k-1]), Not(b[k-1]), Not(a[k]), b[k],
                  ON[k-1], x[k] == x[k-1]))


def t2_t2_ts(a, b, K, h, x, k, d, ON, OFF):
    return Or(And(Not(a[k-1]), b[k-1], Not(a[k]), b[k],
                  x[k-1] >= 20, x[k-1] <= 100,
                  Not(ON[k-1]), Not(OFF[k-1]),
                  x[k] == x[k-1] + d * (K * (h - x[k-1]))),
              And(Not(a[k-1]), b[k-1], a[k], b[k],
                  OFF[k-1], x[k] == x[k-1]),
              And(Not(a[k-1]), b[k-1], a[k], Not(b[k]),
                  x[k-1] == 100, x[k] == x[k-1]))


def t3_t3_ts(a, b, K, h, x, k, d, ON, OFF):
    return Or(And(a[k-1], Not(b[k-1]), a[k], Not(b[k]), x[k-1] == 100,
                  Not(ON[k-1]), Not(OFF[k-1]),
                  x[k] == x[k-1] + d * 0),
              And(a[k-1], Not(b[k-1]), a[k], b[k], OFF[k-1],
                  x[k] == x[k-1]))


def t4_t4_ts(a, b, K, h, x, k, d, ON, OFF):
    return Or(And(a[k-1], b[k-1], a[k], b[k], x[k-1] >= 20,
                  Not(ON[k-1]), Not(OFF[k-1]),
                  x[k-1] <= 100, x[k] == x[k-1] + d * (-K * x[k-1])),
              And(a[k-1], b[k-1], Not(a[k]), Not(b[k]),
                  x[k-1] == 20, x[k] == x[k-1]),
              And(a[k-1], b[k-1], Not(a[k]), b[k],
                  ON[k-1], x[k] == x[k-1]))


# Controller maintaining temperature around 93C, this leads to a stiff
# system.
def c1_c1_c2(c, x, k, d, ON, OFF, T, e):
    return Or(And(Not(c[k-1]), Not(c[k]),
                  Not(ON[k]), Not(OFF[k]),
                  x[k-1] < T),
              And(Not(c[k-1]), c[k], x[k-1] >= T+e, OFF[k],
                  Not(ON[k])))


def c2_c2_c1(c, x, k, d, ON, OFF, T, e):
    return Or(And(c[k-1], c[k], x[k-1] >= T,
                  Not(ON[k]), Not(OFF[k])),
              And(c[k-1], Not(c[k]),
                  x[k-1] < T-e, Not(OFF[k]), ON[k]))


def main(TIMEOUT=60, k=1000):
    # declare the bits to handle state
    a = [Bool('a_%s' % i) for i in range(k+1)]
    b = [Bool('b_%s' % i) for i in range(k+1)]
    x = [Real('x_%s' % i) for i in range(k+1)]

    # delta
    d = Real('d')

    s = SolverFor('QF_NRA')
    s.set('timeout', TIMEOUT)
    s.add(d > 0)
    s.add(d <= 1)
    # Initial conditions
    s.add(x[0] == 20, Not(a[0]), Not(b[0]))

    # The constants
    K = 0.075
    h = 150

    # The discrete signals
    ON = [Bool('ON_%s' % i) for i in range(k+1)]
    OFF = [Bool('OFF_%s' % i) for i in range(k+1)]

    # Initial value of signals
    s.add(And(ON[0], Not(OFF[0])))

    # The controller state
    c = [Bool('c_%s' % i) for i in range(k+1)]
    # Initial state of controller
    s.add(Not(c[0]))

    # The T variable
    T = Real('T')
    s.add(T == 93)

    tt = [Real('tt_%s' % i) for i in range(k+1)]
    s.add(tt[0] == 0)
    s.add(*[tt[i] == tt[i-1] + d for i in range(1, k+1)])
    # The error epsilon
    e = Real('e')
    s.add(e > -1)
    s.add(e < 1)

    # Add the transitions for the watertank
    for i in range(1, k+1):
        s.add(Or(t1_t1_ts(a, b, K, h, x, i, d, ON, OFF),
                 t2_t2_ts(a, b, K, h, x, i, d, ON, OFF),
                 t3_t3_ts(a, b, K, h, x, i, d, ON, OFF),
                 t4_t4_ts(a, b, K, h, x, i, d, ON, OFF)))

        # Now add the controller
        s.add(Or(c1_c1_c2(c, x, i, d, ON, OFF, T, e),
                 c2_c2_c1(c, x, i, d, ON, OFF, T, e)))

    # Add the properties
    # Safety property
    s.add(And(*[x[i] < 100 for i in range(k+1)]))

    # Fairness properties
    # If ON -> <> OFF
    s.add(Implies(ON[0], Or(*[OFF[i] for i in range(1, k)])))
    # if OFF -> <> ON
    s.add(And(*[Implies(OFF[i],
                        Or(*[ON[j]
                             for j in range(i+1, k+1)]))
                for i in range(1, k)]))

    # print(s.sexpr())
    # if s.check() == sat:
    #     print(s.model()[d])
    #     print(s.model()[e])
    #     for i in range(k+1):
    #         print(s.model()[tt[i]].as_decimal(4), ',',
    #               s.model()[x[i]].as_decimal(4))
    # else:
    #     print('unsat')
    return s.check()


if __name__ == '__main__':
    s = timeit.default_timer()
    ret = main(TIMEOUT=100000, k=16)
    et = timeit.default_timer() - s
    print(et, ret)
