#!/usr/bin/env python3

from z3 import And, Real, Bool
from z3 import sat
from z3 import Or, Not, Solver


def t1_t1_t2(k, x, a, b, d, s1, t):
    return Or(And(Not(a[k-1]), Not(b[k-1]), x[k-1] >= 0, x[k-1] <= 5.5,
                  Not(a[k]), Not(b[k]), x[k] == x[k-1] + d * s1,
                  t[k] == t[k-1] + d),
              And(Not(a[k-1]), Not(b[k-1]), x[k-1] >= 5,
                  x[k-1] <= 5.5,
                  x[k] == x[k-1], Not(a[k]), b[k],
                  t[k] == t[k-1]))


def t2_t2_t3(k, x, a, b, d, s2, t, bb):
    return Or(And(Not(a[k-1]), b[k-1], Not(a[k]), b[k],
                  x[k-1] >= 5, x[k-1] <= 15,
                  x[k] == x[k-1] + d * (bb * (t[k-1])),
                  t[k] == t[k-1] + d),
              And(Not(a[k-1]), b[k-1], a[k], Not(b[k]),
                  x[k-1] == 15, x[k] == x[k-1],
                  t[k] == t[k-1]))


def t3_t3_t1(k, x, a, b, d, s3, t):
    return Or(And(a[k-1], Not(b[k-1]), x[k-1] >= 15,
                  x[k-1] <= 25,
                  x[k] == x[k-1] + d * s3,
                  a[k], Not(b[k]), t[k] == t[k-1] + d),
              And(a[k-1], Not(b[k-1]),
                  Not(a[k]), Not(b[k]),
                  x[k-1] == 25, x[k] == 0, t[k] == 0))


def top_top_g1(k, x, UP, DOWN, gs1, gs2, d):
    return Or(And(Not(gs1[k-1]), Not(gs2[k-1]),
                  Not(gs1[k]), Not(gs2[k]),
                  Not(DOWN[k-1]),
                  Or(x[k-1] == 10, UP[k-1]),
                  x[k] == x[k-1]),
              And(Not(gs1[k-1]), Not(gs2[k-1]),
                  gs1[k], gs2[k], Not(UP[k-1]),
                  DOWN[k-1], x[k] == x[k-1]))


def g2_g2_bottom(k, x, UP, DOWN, gs1, gs2, d, fd):
    return Or(And(x[k-1] >= 1, x[k-1] <= 10,
                  gs1[k-1], gs2[k-1], gs1[k], gs2[k],
                  x[k] == x[k-1] + ((1-x[k-1])/2)*d,
                  Not(UP[k-1]), Not(DOWN[k-1])),
              And(gs1[k-1], gs2[k-1], gs1[k], Not(gs2[k]),
                  UP[k-1], Not(DOWN[k-1]), x[k] == x[k-1]),
              And(gs1[k-1], gs2[k-1], Not(gs1[k]), gs2[k],
                  Not(UP[k-1]), Not(DOWN[k-1]),
                  x[k-1] == 1, x[k] == x[k-1]))


def g1_g1_g2(k, x, UP, DOWN, gs1, gs2, d, ff):
    return Or(And(gs1[k-1], Not(gs2[k-1]), Not(UP[k-1]),
                  Not(DOWN[k-1]), gs1[k], Not(gs2[k]),
                  x[k-1] >= 1, x[k-1] <= 10,
                  # Tuning how fast to move the gate up!
                  # x[k] == x[k-1] + (d * (11 - (x[k-1]/2)))),
                  x[k] == x[k-1] + (d * (ff - (x[k-1]/2)))),
              And(gs1[k-1], Not(gs2[k-1]), DOWN[k-1],
                  Not(UP[k-1]),
                  gs1[k], gs2[k], x[k] == x[k-1]),
              And(gs1[k-1], Not(gs2[k-1]), Not(gs1[k]), Not(gs2[k]),
                  Not(DOWN[k-1]), Not(UP[k-1]),
                  x[k-1] == 10,
                  x[k] == x[k-1]))


def bot_bot_g1(k, x, UP, DOWN, gs1, gs2, d):
    return Or(And(Not(gs1[k-1]), gs2[k], Not(gs1[k-1]), gs2[k],
                  Not(UP[k-1]),
                  x[k-1] == 1,
                  x[k] == x[k-1]),
              And(Not(gs1[k-1]), gs2[k-1], gs1[k], Not(gs2[k]),
                  UP[k-1], Not(DOWN[k-1]), x[k] == x[k-1]))


def c1_c1_c2(k, y, UP, DOWN, c1, c2, d):
    return Or(And(y[k-1] < 5, Not(c1[k-1]), Not(c2[k-1]),
                  Not(c2[k]), Not(c1[k]), Not(UP[k]),
                  Not(DOWN[k])),
              And(Not(c1[k-1]), Not(c2[k-1]),
                  Not(c1[k]), c2[k], y[k-1] == 5,
                  DOWN[k], Not(UP[k])))


def c2_c2_c3(k, y, UP, DOWN, c1, c2, d):
    return Or(And(Not(c1[k-1]), c2[k-1], Not(c1[k]), c2[k],
                  y[k-1] >= 5, y[k-1] < 15,
                  Not(UP[k]), Not(DOWN[k])),
              And(Not(c1[k-1]), c2[k-1], c1[k], Not(c2[k]),
                  y[k-1] == 15, UP[k],
                  Not(DOWN[k])))


def c3_c3_c4(k, y, UP, DOWN, c1, c2, d):
    return Or(And(c1[k-1], Not(c2[k-1]), c1[k], Not(c2[k]),
                  y[k-1] < 25, y[k-1] >= 15,
                  Not(UP[k]), Not(DOWN[k])),
              And(c1[k-1], Not(c2[k-1]), c1[k], c2[k],
                  y[k-1] == 25, Not(DOWN[k]), Not(UP[k])))


def c4_c4_c1(k, y, UP, DOWN, c1, c2, d):
    return Or(And(c1[k-1], c2[k-1], c1[k], c2[k],
                  y[k-1] == 25, Not(DOWN[k]), Not(UP[k])),
              And(c1[k-1], c2[k-1], Not(c1[k]), Not(c2[k]),
                  y[k-1] == 0, Not(UP[k]), Not(DOWN[k])))


def main(TIMEOUT=60, k=1000):

    # Now initialize the variables for states for train HA
    a = [Bool('a_%s' % i) for i in range(k+1)]
    b = [Bool('b_%s' % i) for i in range(k+1)]
    y = [Real('y_%s' % i) for i in range(k+1)]

    # The δ size
    d = Real('d')

    # Initialize the solver
    s = Solver()
    s.set('timeout', TIMEOUT)

    # Add the constraints for δ
    s.add(d > 0)
    s.add(d < 1)

    # Now add the train HA into the solver
    # Train initial state
    s.add(And(y[0] == 0, Not(a[0]), Not(b[0])))

    t = [Real('t_%s' % i) for i in range(k+1)]
    s.add(t[0] == 0)
    s2 = Real('s2')
    s.add(s2 > 0)
    bb = Real('bb')
    # Now the rest of the transitions
    for i in range(1, k+1):
        s.add(Or(t1_t1_t2(i, y, a, b, d, 2, t),
                 t2_t2_t3(i, y, a, b, d, s2, t, bb),
                 t3_t3_t1(i, y, a, b, d, 2, t)))

    # These are shared between controller and gate
    UP = [Bool('UP_%s' % i) for i in range(k+1)]
    DOWN = [Bool('DOWN_%s' % i) for i in range(k+1)]

    # These are private to the gate
    gs1 = [Bool('gs1_%s' % i) for i in range(k+1)]
    gs2 = [Bool('gs2_%s' % i) for i in range(k+1)]
    x = [Real('x_%s' % i) for i in range(k+1)]

    # Add the initial state of the gate
    s.add(And(Not(gs1[0]), Not(gs2[0]), x[0] == 10))

    ff, fd = Real('ff'), Real('fd')
    # Add the other transitions
    for i in range(1, k+1):
        s.add(Or(top_top_g1(i, x, UP, DOWN, gs1, gs2, d),
                 g1_g1_g2(i, x, UP, DOWN, gs1, gs2, d, ff),
                 g2_g2_bottom(i, x, UP, DOWN, gs1, gs2, d, fd),
                 bot_bot_g1(i, x, UP, DOWN, gs1, gs2, d)))

    # Now add the controller
    c1 = [Bool('c1_%s' % i) for i in range(k+1)]
    c2 = [Bool('c2_%s' % i) for i in range(k+1)]

    # Initial state
    s.add(And(Not(c1[0]), Not(c2[0]), Not(UP[0]), Not(DOWN[0])))

    # Now the other transitions
    for i in range(1, k+1):
        s.add(Or(c1_c1_c2(i, y, UP, DOWN, c1, c2, d),
                 c2_c2_c3(i, y, UP, DOWN, c1, c2, d),
                 c3_c3_c4(i, y, UP, DOWN, c1, c2, d),
                 c4_c4_c1(i, y, UP, DOWN, c1, c2, d)))

    # This is enforcing liveness
    s.add(Or(*[And(c1[i], c2[i])
               for i in range(k+1)]))
    # Another property that the gate actually goes down
    s.add(Or(*[And(x[i] <= 2, y[i] >= 5, y[i] <= 15)
               for i in range(k+1)]))
    # This is enforcing safety
    s.add(And(*[Not(And(y[i] == 0, x[i] < 10))
                for i in range(k+1)]))

    # Print the sexpression
    # print(s.sexpr())

    if (s.check() == sat):
        print('d:', s.model()[d])
        print('v:', s.model()[ff])
        print('bb:', s.model()[bb])
        # print('s2:', s.model()[s2])
        print('gs1;gs2,c1;c2,t1;t2,DOWN,UP,y,x,t')
        for i in range(k+1):
            print(s.model()[gs1[i]], ';', s.model()[gs2[i]], ',',
                  s.model()[c1[i]], ';', s.model()[c2[i]], ',',
                  s.model()[a[i]], ';', s.model()[b[i]], ',',
                  s.model()[DOWN[i]], ',', s.model()[UP[i]], ',',
                  s.model()[y[i]], ',', s.model()[x[i]], ',',
                  s.model()[t[i]])
    else:
        print('unsat')
    # return s.check()


if __name__ == '__main__':
    main(TIMEOUT=100000, k=35)
