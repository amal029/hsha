#!/usr/bin/env python3

from z3 import And, Real, sat
from z3 import Or, SolverFor
import sympy as S
import operator as OP
from functools import reduce


def to_z3(expr, toreplace={}):
    if expr.args == ():
        # Check if we need to replace something here
        if str(expr) in toreplace.keys():
            expr = toreplace[str(expr)]
        return expr
    else:
        cargs = [to_z3(a, toreplace) for a in expr.args]
        cargs = [a for a in cargs if a is not None]
        # print(cargs)
        if expr.func == S.Mul:
            return reduce(OP.mul, cargs)
        elif expr.func == S.Add:
            return reduce(OP.add, cargs)
        elif expr.func == S.Pow:
            return reduce(OP.pow, cargs)


def main(k=4):
    ts = [Real('t_%s' % i) for i in range(k)]
    xs = [Real('x_%s' % i) for i in range(k)]
    cs = [Real('c_%s' % i) for i in range(k)]
    s = SolverFor('QF_NRA')
    # First thing all t's are greater than 0
    for t in ts:
        s.add(t >= 0.0)
    # Next the x[0] is 0.0 to start with
    # This is the initial value of x
    s.add(xs[0] == 1.0)

    # Location 1 ODE
    # This is the ode in the first location
    # dx/dt = x
    o1 = S.sympify("diff(x(t))-x(t)")
    s1 = S.dsolve(o1)
    # The taylor expansion
    s1 = S.series(s1.args[1], S.sympify('t'), n=6)
    # This is the ode in the second location
    # dx/dt = -0.2*x
    o2 = S.sympify('diff(x(t))+0.2*x(t)')
    s2 = S.dsolve(o2)
    s2 = S.series(s2.args[1], S.sympify('t'), n=6)

    # These are the expressions for the three things
    z1 = to_z3(s1, toreplace={'C1': cs[0], 't': ts[0]})
    z2 = to_z3(s2, toreplace={'C1': cs[1], 't': ts[1]})
    z3 = to_z3(s1, toreplace={'C1': cs[2], 't': ts[2]})

    first_expr = And(xs[0] == cs[0], xs[1] == z1, xs[1] >= 95)
    second_expr = And(xs[1] == cs[1], xs[2] == z2, xs[2] <= 90)
    third_expr = And(xs[2] == cs[2], xs[3] == z3, xs[3] >= 95)

    # Let us put a safety propoerty
    # G (x <= 100)
    safety_prop = And([x <= 100 for x in xs])

    # Now let us put a liveness property with time in there. This
    # property states that globally in the future the temperature
    # becomes at least 20 within 5.5 seconds – timed ltl (so to speak).

    liveness_prop = Or(And(xs[1] >= 20, ts[0] >= 0, ts[0] <= 5.5),
                       And(xs[2] >= 20, (ts[0] + ts[1]) >= 0,
                           (ts[0] + ts[1]) <= 5.5),
                       And(xs[3] >= 20, (ts[1] + ts[0] + ts[2]) >= 0,
                           (ts[1] + ts[2] + ts[0]) <= 5.5))

    # Now make the final And
    final = And(first_expr, second_expr, third_expr, safety_prop,
                liveness_prop)
    s.add(final)
    ret = s.check()
    if ret == sat:
        for i in range(k):
            print(str(ts[i]), s.model()[ts[i]])
            print(str(xs[i]), s.model()[xs[i]])


if __name__ == '__main__':
    main(4)
