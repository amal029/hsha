#!/usr/bin/env python3

import timeit
import csv
import bb as BB
import multiplie_room_thermostats_dreach as TH
import train_gate_controller as TG
import waterTank as WT


def main(TIMEOUT=60, ITERS=100):
    # dictionary for holding the time values
    d = {'bb-'+str(i): None for i in range(ITERS)}
    d.update({'tg-'+str(i): None for i in range(ITERS)})
    d.update({'th-'+str(i): None for i in range(ITERS)})
    d.update({'wt-'+str(i): None for i in range(ITERS)})

    # Do the bb example first
    for i in range(ITERS):
        st = timeit.default_timer()
        ret = BB.main(TIMEOUT, i)
        et = timeit.default_timer() - st
        d['bb-'+str(i)] = (et, ret)

    # Do the TH example first
    for i in range(ITERS):
        st = timeit.default_timer()
        ret = TH.main(TIMEOUT, i)
        et = timeit.default_timer() - st
        d['th-'+str(i)] = (et, ret)

    # Do the TG example first
    for i in range(ITERS):
        st = timeit.default_timer()
        ret = TG.main(TIMEOUT, i)
        et = timeit.default_timer() - st
        d['tg-'+str(i)] = (et, ret)

    # Do the WT example first
    for i in range(ITERS):
        st = timeit.default_timer()
        ret = WT.main(TIMEOUT, i)
        et = timeit.default_timer() - st
        d['wt-'+str(i)] = (et, ret)
    print(d)

    # Write the csv file
    with open('output.csv', 'w') as f:
        w = csv.DictWriter(f, ['Benchmark', 'iter_num', 'time_taken',
                               'SAT'])
        w.writeheader()
        for k, v in d.items():
            ss = k.split('-')
            b = ss[0]
            i = ss[1]
            (x, y) = v
            w.writerow({'Benchmark': b, 'iter_num': i,
                        'time_taken': x, 'SAT': y})


if __name__ == '__main__':
    TIMEOUT = 10
    ITERS = 5
    main(TIMEOUT, ITERS)
