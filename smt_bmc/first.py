#!/usr/bin/env python3

from z3 import And, Real, Bool, sat
from z3 import Or, Not, Solver


def s1_s1_s0(k, x, a, δ, s2):
    return (Or(
        And(x[k-1] <= 0.0,
            x[k] == 0.0,
            (a[k-1]),
            Not(a[k])),
        And(a[k-1],
            a[k],
            (x[k-1] >= 0.0),
            (x[k] == δ*s2 + x[k-1]))))


def s0_s0_s1(k, x, a, δ, s1):
    return (Or(
        And(x[k-1] >= 5.0,
            x[k] == 5.0,
            Not(a[k-1]),
            a[k]),
        And(Not(a[k-1]),
            Not(a[k]),
            (x[k-1] <= 5.0),
            (x[k] == δ*s1 + x[k-1]))))


def main(k=10):
    # delta
    s1 = 1
    s2 = -1
    # Find a δ such that system is not violated
    δ = Real('d')
    # Declare the variable to simulate the state
    a = [Bool('a_%s' % i)
         for i in range(k+1)]

    # Declare the continous variable
    x = [Real('x_%s' % i)
         for i in range(k+1)]

    s = Solver()
    # Constraints on δ
    s.add(δ >= 0.001)
    s.add(δ <= 1)
    # Now start building the transition system
    # Initial state
    s.add(And(Not(a[0]), (x[0] == 1.0)))

    # Transition from s0 -- s0 or s0 -- s1
    s.add(s0_s0_s1(1, x, a, δ, s1))
    # Both these are possible in this system
    for i in range(2, k+1):
        s.add(Or(s0_s0_s1(i, x, a, δ, s1),
                 (s1_s1_s0(i, x, a, δ, s2))))

    # Add a property
    s.add(Or(*[x[i] == 0
               for i in range(0, k+1)]))
    # print(s.sexpr())

    res = s.check()
    print(res)
    if res == sat:
        print('δ:', s.model()[δ])


if __name__ == '__main__':
    main(k=100)
