#!/usr/bin/env python3

from z3 import Or
from gensmt import gen_smt as smt


def main(TIMEOUT=60, k=1000, f='st.dot'):
    s, d, rdict = smt(f=f, k=k)
    print(s)
    s.add(d < 1.0)
    s.add(Or(*[rdict['ST']['states']['t2'][i]
               for i in range(1, k+1)]))
    ret = s.check()
    # if ret == sat:
    print(s.model()[d])
    # for i in range(k+1):
    #     print(s.model()[vars['t'][i]].as_decimal(9),
    #           ',', s.model()[vars['x1'][i]].as_decimal(9),
    #           ',', s.model()[vars['x2'][i]].as_decimal(9))
    print(ret)
    return ret


main(f='st.dot', k=3)
