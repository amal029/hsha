#!/usr/bin/env python3

from z3 import And, Real, Bool, sat, Implies
from z3 import Or, Not, SolverFor
import timeit


def ON0_ON0_OFF0(x0, x1, x2, d, a, K, c, i, Tm, TM, h):
    return Or(And(Not(a[i-1]), Not(a[i]), x0[i-1] >= Tm, x0[i-1] <= TM,
                  x0[i] == x0[i-1] + d * (K*(h - ((1-(2*c))*x0[i-1]
                                                  + c*x2[i-1]
                                                  + c*x1[i-1])))),
              And(Not(a[i-1]), a[i], x0[i-1] == TM, x0[i] == x0[i-1]))


# XXX
def OFF0_OFF0_ON0(x0, x1, x2, d, a, K, c, i, Tm, TM, h):
    return Or(And(a[i-1], a[i],
                  x0[i-1] >= Tm, x0[i-1] <= TM,
                  x0[i] == x0[i-1] + d * -1
                  # (-K * ((1-2*c)*x0[i-1] + c*x2[i-1] + c*x1[i-1]))
    ),
              And(a[i-1], Not(a[i]), x0[i-1] == Tm, x0[i] == x0[i-1]))


def ON1_ON1_OFF1(x0, x1, x2, d, a, K, c, i, Tm, TM, h):
    return Or(And(Not(a[i-1]), Not(a[i]), x1[i-1] >= Tm, x1[i-1] <= TM,
                  x1[i] == x1[i-1] + d * (K*(h - ((1-2*c)*x1[i-1] + c*x0[i-1]
                                                  + c*x2[i-1])))),
              And(Not(a[i-1]), a[i], x1[i-1] == TM, x1[i] == x1[i-1]))


# XXX
def OFF1_OFF1_ON1(x0, x1, x2, d, a, K, c, i, Tm, TM, h):
    return Or(And(a[i-1], a[i], x1[i-1] >= Tm, x1[i-1] <= TM,
                  x1[i] == x1[i-1] + d * -1
                  # (-K * ((1-2*c)*x1[i-1] + c*x0[i-1] + c*x2[i-1]))
    ),
              And(a[i-1], Not(a[i]), x1[i-1] == Tm, x1[i] == x1[i-1]))


def ON2_ON2_OFF2(x0, x1, x2, d, a, K, c, i, Tm, TM, h):
    return Or(And(Not(a[i-1]), Not(a[i]), x2[i-1] >= Tm, x2[i-1] <= TM,
                  x2[i] == x2[i-1] + d * (K*(h - ((1-2*c)*x2[i-1] + c*x1[i-1]
                                                  + c*x0[i-1])))),
              And(Not(a[i-1]), a[i], x2[i-1] == TM, x2[i] == x2[i-1]))


# XXX
def OFF2_OFF2_ON2(x0, x1, x2, d, a, K, c, i, Tm, TM, h):
    return Or(And(a[i-1], a[i], x2[i-1] >= Tm, x2[i-1] <= TM,
                  x2[i] == x2[i-1] + d * -1
                  # (-K * ((1-2*c)*x2[i-1] + c*x1[i-1] + c*x0[i-1]))
    ),
              And(a[i-1], Not(a[i]), x2[i-1] == Tm, x2[i] == x2[i-1]))


def main(TIMEOUT=60, k=1000):
    s = SolverFor('QF_NRA')
    s.set('timeout', TIMEOUT)
    # The state variables
    a = [Bool('a_%s' % i) for i in range(k+1)]
    b = [Bool('b_%s' % i) for i in range(k+1)]
    c = [Bool('c_%s' % i) for i in range(k+1)]
    a1 = [Bool('a1_%s' % i) for i in range(k+1)]
    a2 = [Bool('a2_%s' % i) for i in range(k+1)]
    a3 = [Bool('a3_%s' % i) for i in range(k+1)]
    a4 = [Bool('a4_%s' % i) for i in range(k+1)]
    a5 = [Bool('a5_%s' % i) for i in range(k+1)]

    # The continous variables
    x0 = [Real('x0_%s' % i) for i in range(k+1)]
    x1 = [Real('x1_%s' % i) for i in range(k+1)]
    x2 = [Real('x2_%s' % i) for i in range(k+1)]
    x4 = [Real('x4_%s' % i) for i in range(k+1)]
    x5 = [Real('x5_%s' % i) for i in range(k+1)]
    x6 = [Real('x6_%s' % i) for i in range(k+1)]
    x7 = [Real('x7_%s' % i) for i in range(k+1)]
    x8 = [Real('x8_%s' % i) for i in range(k+1)]

    # The constants
    K0, K1, K2 = 0.015, 0.045, 0.03
    h0, h1, h2 = 100, 200, 300

    # Tm and TM
    Tm, TM = 10, 21

    # The d (delta)
    d = Real('d')
    s.add(And(d > 0))

    # Now the initial conditions
    s.add(Or(Not(a[0]), Not(b[0]), Not(c[0]),
             Not(a1[0]), Not(a2[0]),
             Not(a3[0]), Not(a4[0]),
             Not(a5[0])))  # starting states
    # s.add(And(x0[0] == Tm, x1[0] == Tm, x2[0] == Tm))
    s.add(And(x0[0] > Tm, x0[0] <= TM))
    s.add(And(x1[0] > Tm, x1[0] <= TM))
    s.add(And(x2[0] > Tm, x2[0] <= TM))
    s.add(And(x4[0] > Tm, x4[0] <= TM))
    s.add(And(x5[0] > Tm, x5[0] <= TM))
    s.add(And(x6[0] > Tm, x6[0] <= TM))
    s.add(And(x7[0] > Tm, x7[0] <= TM))
    s.add(And(x8[0] > Tm, x8[0] <= TM))

    # Tunable variable?
    cc = Real('cc')
    s.add(cc > 0)
    # s.add(cc == 0.0001)

    # Now make the transitions
    for i in range(1, k+1):
        s.add(Or(ON0_ON0_OFF0(x0, x1, x2, d, a, K0, cc, i, Tm, TM, h0),
                 OFF0_OFF0_ON0(x0, x1, x2, d, a, K0, cc, i, Tm, TM, h0)))

        s.add(Or(ON1_ON1_OFF1(x0, x1, x2, d, b, K1, cc, i, Tm, TM, h1),
                 OFF1_OFF1_ON1(x0, x1, x2, d, b, K1, cc, i, Tm, TM, h1)))

        s.add(Or(ON2_ON2_OFF2(x0, x1, x2, d, c, K2, cc, i, Tm, TM, h2),
                 OFF2_OFF2_ON2(x0, x1, x2, d, c, K2, cc, i, Tm, TM, h2)))

        # The fourth one
        s.add(Or(ON2_ON2_OFF2(x4, x1, x2, d, a1, K2, cc, i, Tm, TM, h2),
                 OFF2_OFF2_ON2(x4, x1, x2, d, a1, K2, cc, i, Tm, TM, h2)))
        # fifth
        s.add(Or(ON2_ON2_OFF2(x5, x1, x2, d, a2, K2, cc, i, Tm, TM, h0),
                 OFF2_OFF2_ON2(x5, x1, x2, d, a2, K2, cc, i, Tm, TM, h0)))
        # Sixth
        s.add(Or(ON2_ON2_OFF2(x6, x1, x2, d, a3, K2, cc, i, Tm, TM, h2),
                 OFF2_OFF2_ON2(x6, x1, x2, d, a3, K2, cc, i, Tm, TM, h2)))
        # Seventh
        s.add(Or(ON2_ON2_OFF2(x7, x1, x2, d, a4, K2, cc, i, Tm, TM, h2),
                 OFF2_OFF2_ON2(x7, x1, x2, d, a4, K2, cc, i, Tm, TM, h2)))
        # Eighth
        s.add(Or(ON2_ON2_OFF2(x8, x1, x2, d, a5, K2, cc, i, Tm, TM, h1),
                 OFF2_OFF2_ON2(x8, x1, x2, d, a5, K2, cc, i, Tm, TM, h1)))

    tt = [Real('tt_%s' % i) for i in range(k+1)]
    s.add(tt[0] == 0)
    s.add(*[tt[i] == tt[i-1] + d for i in range(1, k+1)])

    # Just the safety property
    s.add(And(*[And(x0[i] >= Tm, x0[i] <= TM) for i in range(k+1)]))
    s.add(And(*[And(x1[i] >= Tm, x1[i] <= TM) for i in range(k+1)]))
    s.add(And(*[And(x2[i] >= Tm, x2[i] <= TM) for i in range(k+1)]))
    s.add(And(*[And(x4[i] >= Tm, x4[i] <= TM) for i in range(k+1)]))
    s.add(And(*[And(x5[i] >= Tm, x5[i] <= TM) for i in range(k+1)]))
    s.add(And(*[And(x6[i] >= Tm, x6[i] <= TM) for i in range(k+1)]))
    s.add(And(*[And(x7[i] >= Tm, x6[i] <= TM) for i in range(k+1)]))
    s.add(And(*[And(x8[i] >= Tm, x6[i] <= TM) for i in range(k+1)]))

    # Liveness property
    s.add(Implies(Not(a[0]), Or(*[a[i] for i in range(1, k)])))
    s.add(Implies(Not(b[0]), Or(*[b[i] for i in range(1, k)])))
    s.add(Implies(Not(c[0]), Or(*[c[i] for i in range(1, k)])))
    s.add(Implies(Not(a1[0]), Or(*[a1[i] for i in range(1, k)])))
    s.add(Implies(Not(a2[0]), Or(*[a2[i] for i in range(1, k)])))
    s.add(Implies(Not(a3[0]), Or(*[a3[i] for i in range(1, k)])))
    s.add(Implies(Not(a4[0]), Or(*[a4[i] for i in range(1, k)])))
    s.add(Implies(Not(a5[0]), Or(*[a5[i] for i in range(1, k)])))

    # Implication
    s.add(And(*[Implies(a[i],
                        Or(*[Not(a[j])
                             for j in range(i+1, k+1)]))
                for i in range(1, k)]))
    s.add(And(*[Implies(b[i],
                        Or(*[Not(b[j])
                             for j in range(i+1, k+1)]))
                for i in range(1, k)]))
    s.add(And(*[Implies(c[i],
                        Or(*[Not(c[j])
                             for j in range(i+1, k+1)]))
                for i in range(1, k)]))

    s.add(And(*[Implies(a1[i],
                        Or(*[Not(a1[j])
                             for j in range(i+1, k+1)]))
                for i in range(1, k)]))
    s.add(And(*[Implies(a2[i],
                        Or(*[Not(a2[j])
                             for j in range(i+1, k+1)]))
                for i in range(1, k)]))
    s.add(And(*[Implies(a3[i],
                        Or(*[Not(a3[j])
                             for j in range(i+1, k+1)]))
                for i in range(1, k)]))
    s.add(And(*[Implies(a4[i],
                        Or(*[Not(a4[j])
                             for j in range(i+1, k+1)]))
                for i in range(1, k)]))
    s.add(And(*[Implies(a5[i],
                        Or(*[Not(a5[j])
                             for j in range(i+1, k+1)]))
                for i in range(1, k)]))
    # Check the model
    # print(s.sexpr())
    ee = s.check()
    # if ee == sat:
    #     print(s.model()[d], s.model()[cc])
    #     for i in range(k+1):
    #         print(s.model()[x0[i]], ',', s.model()[x1[i]],
    #               s.model()[x2[i]], ',', s.model()[tt[i]])
    # else:
    #     print(ee)
    return ee


if __name__ == '__main__':
    s = timeit.default_timer()
    ret = main(TIMEOUT=60000, k=4)
    et = timeit.default_timer() - s
    print(et, ret)
