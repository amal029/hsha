## To compile the BouncingBall HIOA (in file BouncingBall.hs) do the
## following:

``` bash
Prompt> cd src/examples/BouncingBall/
Prompt> ghci -i../../language BouncingBall.hs
*BouncingBall> main
*BouncingBall> :q
```

## The above should generate bouncingBall.c file
## Now do

``` bash
Prompt> gcc bouncingBall.c bouncing_ball_io.c Main.c -o bb
./bb
./bb_plot.py bouncing_ball.csv # This should plot the result of the simulation
```

# Dependencies
1. Haskell >= 7.10.3