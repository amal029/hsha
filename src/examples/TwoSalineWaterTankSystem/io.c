#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<string.h>
#include<assert.h>

/* The files are in csv format */
#define OFILE "wout.csv"
#define IFILE "winr.csv"

#define TRUE 1
#define FALSE 0

/* The step size */
double d = 0.01;

/* The events to read from file */
extern double Tank1_ox1, Tank1_oq1, Tank2_ox2, Tank2_oq2;
unsigned char Tank1_ON1, Tank1_OFF1, Tank2_ON2, Tank2_OFF2, Controller_OFF1, Controller_ON1,
  Controller_OFF2, Controller_ON2;

/* The tick counter */
/* It is static to hide it inside this file */
static size_t tick = 0;

/* The output file pointer */
FILE *fo = NULL;


/* Read input x from file */
void readInput() {}

/* Write output x to file */
void writeOutput(){
  static unsigned char count = 0;
  if (0 == count){
    fo = fopen(OFILE, "w");
    if (fo == NULL){
      perror(OFILE);
      exit(1);
    }
    ++count;
    fprintf(fo, "title_x = %s\ntitle = %s\ntitle_y = %s\n","Ticks","Evolution of Salt in Water", "Salt");
    /* fprintf(fo,"%s,%s,%s,%s,%s\n", "Tick", "Tank1_ox1", "Tank1_oq1", "Tank2_ox2", "Tank2_oq2"); */
  }
  fprintf(fo, "%0.2f,%0.2f,%0.2f,%0.2f,%0.2f\n", (tick++)*d, Tank1_ox1, Tank1_oq1, Tank2_ox2, Tank2_oq2);
  /* fprintf(fo, "%zu,CON1:%d,COFF2:%d,COFF1:%d,CON2:%d,T1ON1:%d,T2OFF2:%d,T1OFF1:%d,T2ON2:%d\n", */
  /* 	  tick, Controller_ON1, Controller_OFF2, Controller_OFF1, Controller_ON2, */
  /* 	  Tank1_ON1, Tank2_OFF2, Tank1_OFF1, Tank2_ON2); */
  if(5000 == tick) {
    printf("DONE!\n");
    exit(1);
  }
  /* printf("%zu,%f,%f,%f,%f\n", tick++, Tank1_ox1, Tank1_oq1, Tank2_ox2, Tank2_oq2); */
}

