module Parabola where
import Language
import qualified Hshac as H
import Prelude hiding ((==),(/=), LT, GT, (<=), (>=), (<), (>), (&&), not)
import Text.PrettyPrint.ANSI.Leijen
import GHC.IO.Handle
import System.IO

dxdt :: Diff
dxdt = DiffT (S "x")

dydt :: Diff
dydt = DiffT (S "y")

odex :: Ode
odex = Ode dxdt 1 (-10)

odey :: Ode
odey = Ode dydt (2 * A(S "x")) 100

assigny :: Update (External Output V)
assigny = OutputVar (S "y") := A (S "y")

t1 :: Loc
t1 = Loc (L "t1") [odex, odey] 
                  [InvariantLoc dxdt (T(S "x") <= TR 0)]
                  ([], [assigny])
                  
t2 :: Loc
t2 = Loc (L "t2") [odex, odey] 
                  [InvariantLoc dxdt (T (S "x") <= TR 10 && (T (S "x") >= TR 0))]
                  ([], [assigny])
e1 :: Edge
e1 = Edge t1 t2 [InvariantEdge ((T(S "x") == TR 0))] 
                ([(S "x") := (A (S "x")), 
                  (S "y") := (A (S "y"))], 
                 [assigny], []) 
                []

parabola :: Ha
parabola = Ha (L "parabola") [t1, t2] t1 [e1] 
               [SymbolDecl (S "x") (-10), SymbolDecl (S "y") 100] [] [OutputVarDecl (S "y") 100]
               [] []

main :: IO ()
main = do 
       h <- openFile "parabola.c" WriteMode
       x <- openFile "Main.c" WriteMode
       let (dd, others) = H.compileReaction parabola
           (oo1, oo2) = others
       hPutDoc h dd
       hPutDoc x (H.mkMain (LastHa parabola) [] [oo1] [oo2])
       hClose x
       hClose h

