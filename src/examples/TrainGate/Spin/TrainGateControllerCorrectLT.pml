int train_pos = 0 ;
int gate_pos = 10 ;
c_decl {\#include<math.h>};
c_decl{extern double d;};
bool Controller_tick = false;
bool Train_tick = false;
bool Gate_tick = false;

mtype Controller_cstate , Controller_pstate ;
bool Controller_force_init_update ;

mtype Train_cstate , Train_pstate ;
bool Train_force_init_update ;

mtype Gate_cstate , Gate_pstate ;
bool Gate_force_init_update ;


c_decl {


};
c_decl { double x_u, x_slope, x_init, x =  10 ;};
c_track "&x" "sizeof(double)";
c_track "&x_slope" "sizeof(double)";
c_track "&x_u" "sizeof(double)";
c_track "&x_init" "sizeof(double)";
c_decl {double  Gate_x = 10.0 ;};
c_track "&Gate_x" "sizeof(double)";


bool Gate_UP ;
bool Gate_DOWN ;
mtype { Gate_g1, Gate_g2, Gate_bottom, Gate_top };
ltl p1 {[]((Gate_DOWN -> <> Gate_UP) && (Gate_UP -> <> Gate_DOWN))};
proctype Gate () {
G1:
   if
    :: (c_expr { ((x >= 1.0) && (x < 10.0)) }  &&  (! Gate_UP)
     && 
    (! Gate_DOWN)  &&  Gate_cstate == Gate_g1) ->
     if
      :: Gate_pstate != Gate_cstate || Gate_force_init_update ->
       c_code { x_init = x ; };
      :: else -> true
     fi;
     d_step {
      c_code {
       x_slope =  (11 + (- (x / 2))) ;
       x_u = (x_slope * d) + x ;
       x_u = (x_u <  1.0  && signbit(x_slope) &&  x_init >  1.0)  ?  1.0 : x_u ;
       x_u = (x_u >  10.0  && !signbit(x_slope) &&  x_init <  10.0)  ?  10.0 : x_u ;
       
       Gate_x = x ;
      };
      Gate_pstate = Gate_cstate;
      Gate_cstate = Gate_g1;
      Gate_force_init_update = false;
      
      Gate_UP  = false;
      Gate_DOWN  = false;
      Gate_tick = true;
     };
     do
      :: Gate_tick -> true
      :: else -> goto  G1
     od;
     ::  (c_expr { (x == 10.0) }
           && Gate_cstate == Gate_g1)  -> 
      d_step {
            c_code {  x_u = x ; };
            c_code {  Gate_x = x ; };
            Gate_pstate =  Gate_cstate ;
            Gate_cstate =  Gate_top ;
            Gate_force_init_update = false;
            Gate_tick = true;
      };
      do
       :: Gate_tick -> true
       :: else -> goto  Top
      od;
     ::  (c_expr { 1 } && 
          Gate_DOWN && 
          Gate_cstate == Gate_g1)  -> 
      d_step {
            c_code {  x_u = x ; };
            c_code {  Gate_x = x ; };
            Gate_pstate =  Gate_cstate ;
            Gate_cstate =  Gate_g2 ;
            Gate_force_init_update = false;
            Gate_tick = true;
      };
      do
       :: Gate_tick -> true
       :: else -> goto  G2
      od;

     :: else -> true
   fi;
G2:
   if
    :: (c_expr { ((x > 1.0) && (x <= 10.0)) }  &&  (! Gate_UP)
     && 
    (! Gate_DOWN)  &&  Gate_cstate == Gate_g2) ->
     if
      :: Gate_pstate != Gate_cstate || Gate_force_init_update ->
       c_code { x_init = x ; };
      :: else -> true
     fi;
     d_step {
      c_code {
       x_slope =  (- (x / 2)) ;
       x_u = (x_slope * d) + x ;
       x_u = (x_u <  1.0  && signbit(x_slope) &&  x_init >  1.0)  ?  1.0 : x_u ;
       x_u = (x_u >  10.0  && !signbit(x_slope) &&  x_init <  10.0)  ?  10.0 : x_u ;
       
       Gate_x = x ;
      };
      Gate_pstate = Gate_cstate;
      Gate_cstate = Gate_g2;
      Gate_force_init_update = false;
      
      Gate_UP  = false;
      Gate_DOWN  = false;
      Gate_tick = true;
     };
     do
      :: Gate_tick -> true
      :: else -> goto  G2
     od;
     ::  (c_expr { (x == 1.0) }
           && Gate_cstate == Gate_g2)  -> 
      d_step {
            c_code {  x_u = x ; };
            c_code {  Gate_x = x ; };
            Gate_pstate =  Gate_cstate ;
            Gate_cstate =  Gate_bottom ;
            Gate_force_init_update = false;
            Gate_tick = true;
      };
      do
       :: Gate_tick -> true
       :: else -> goto  Bottom
      od;
     ::  (c_expr { 1 } && 
          Gate_UP && 
          Gate_cstate == Gate_g2)  -> 
      d_step {
            c_code {  x_u = x ; };
            c_code {  Gate_x = x ; };
            Gate_pstate =  Gate_cstate ;
            Gate_cstate =  Gate_g1 ;
            Gate_force_init_update = false;
            Gate_tick = true;
      };
      do
       :: Gate_tick -> true
       :: else -> goto  G1
      od;

     :: else -> true
   fi;
Bottom:
   if
    :: (c_expr { (x == 1.0) }  &&  (! Gate_UP)
     && 
    (! Gate_DOWN)  &&  Gate_cstate == Gate_bottom) ->
     if
      :: Gate_pstate != Gate_cstate || Gate_force_init_update ->
       c_code { x_init = x ; };
      :: else -> true
     fi;
     d_step {
      c_code {
       x_slope =  0 ;
       x_u = (x_slope * d) + x ;
       x_u = (x_u <  1.0  && signbit(x_slope) &&  x_init >  1.0)  ?  1.0 : x_u ;
       x_u = (x_u >  1.0  && !signbit(x_slope) &&  x_init <  1.0)  ?  1.0 : x_u ;
       
       Gate_x = x ;
      };
      Gate_pstate = Gate_cstate;
      Gate_cstate = Gate_bottom;
      Gate_force_init_update = false;
      
      Gate_UP  = false;
      Gate_DOWN  = false;
      Gate_tick = true;
     };
     do
      :: Gate_tick -> true
      :: else -> goto  Bottom
     od;
     ::  (c_expr { 1 } && 
          Gate_DOWN && 
          Gate_cstate == Gate_bottom)  -> 
      d_step {
            c_code {  x_u = x ; };
            c_code {  Gate_x = x ; };
            Gate_pstate =  Gate_cstate ;
            Gate_cstate =  Gate_bottom ;
            Gate_force_init_update = true;
            Gate_tick = true;
      };
      do
       :: Gate_tick -> true
       :: else -> goto  Bottom
      od;
     ::  (c_expr { 1 } && 
          Gate_UP && 
          Gate_cstate == Gate_bottom)  -> 
      d_step {
            c_code {  x_u = x ; };
            c_code {  Gate_x = x ; };
            Gate_pstate =  Gate_cstate ;
            Gate_cstate =  Gate_g1 ;
            Gate_force_init_update = false;
            Gate_tick = true;
      };
      do
       :: Gate_tick -> true
       :: else -> goto  G1
      od;

     :: else -> true
   fi;
Top:
   if
    :: (c_expr { (x == 10.0) }  &&  (! Gate_UP)
     && 
    (! Gate_DOWN)  &&  Gate_cstate == Gate_top) ->
     if
      :: Gate_pstate != Gate_cstate || Gate_force_init_update ->
       c_code { x_init = x ; };
      :: else -> true
     fi;
     d_step {
      c_code {
       x_slope =  0 ;
       x_u = (x_slope * d) + x ;
       x_u = (x_u <  10.0  && signbit(x_slope) &&  x_init >  10.0)  ?  10.0 : x_u ;
       x_u = (x_u >  10.0  && !signbit(x_slope) &&  x_init <  10.0)  ?  10.0 : x_u ;
       
       Gate_x = x ;
      };
      Gate_pstate = Gate_cstate;
      Gate_cstate = Gate_top;
      Gate_force_init_update = false;
      
      Gate_UP  = false;
      Gate_DOWN  = false;
      Gate_tick = true;
     };
     do
      :: Gate_tick -> true
      :: else -> goto  Top
     od;
     ::  (c_expr { 1 } && 
          Gate_UP && 
          Gate_cstate == Gate_top)  -> 
      d_step {
            c_code {  x_u = x ; };
            c_code {  Gate_x = x ; };
            Gate_pstate =  Gate_cstate ;
            Gate_cstate =  Gate_top ;
            Gate_force_init_update = true;
            Gate_tick = true;
      };
      do
       :: Gate_tick -> true
       :: else -> goto  Top
      od;
     ::  (c_expr { 1 } && 
          Gate_DOWN && 
          Gate_cstate == Gate_top)  -> 
      d_step {
            c_code {  x_u = x ; };
            c_code {  Gate_x = x ; };
            Gate_pstate =  Gate_cstate ;
            Gate_cstate =  Gate_g2 ;
            Gate_force_init_update = false;
            Gate_tick = true;
      };
      do
       :: Gate_tick -> true
       :: else -> goto  G2
      od;

     :: else -> true
   fi;
}
c_decl {


};
c_decl { double y_u, y_slope, y_init, y =  0 ;};
c_track "&y" "sizeof(double)";
c_track "&y_slope" "sizeof(double)";
c_track "&y_u" "sizeof(double)";
c_track "&y_init" "sizeof(double)";
c_decl {double  Train_y = 0.0 ;};
c_track "&Train_y" "sizeof(double)";



mtype { Train_t1, Train_t2, Train_t3 };
proctype Train () {
T1:
   if
    :: (c_expr { ((y < 5.0) && (y >= 0.0)) }  &&  true  &&  Train_cstate == Train_t1) ->
     if
      :: Train_pstate != Train_cstate || Train_force_init_update ->
       c_code { y_init = y ; };
      :: else -> true
     fi;
     d_step {
      c_code {
       y_slope =  1 ;
       y_u = (y_slope * d) + y ;
       y_u = (y_u <  0.0  && signbit(y_slope) &&  y_init >  0.0)  ?  0.0 : y_u ;
       y_u = (y_u >  5.0  && !signbit(y_slope) &&  y_init <  5.0)  ?  5.0 : y_u ;
       
       Train_y = y ;
      };
      Train_pstate = Train_cstate;
      Train_cstate = Train_t1;
      Train_force_init_update = false;
      
      
      Train_tick = true;
     };
     do
      :: Train_tick -> true
      :: else -> goto  T1
     od;
     ::  (c_expr { (y == 5.0) }
           && 
          Train_cstate == Train_t1)  -> 
      d_step {
            c_code {  y_u = y ; };
            c_code {  Train_y = y ; };
            Train_pstate =  Train_cstate ;
            Train_cstate =  Train_t2 ;
            Train_force_init_update = false;
            Train_tick = true;
      };
      do
       :: Train_tick -> true
       :: else -> goto  T2
      od;

     :: else -> true
   fi;
T2:
   if
    :: (c_expr { ((y < 15.0) && (y >= 5.0)) }  &&  true  &&  Train_cstate == Train_t2) ->
     if
      :: Train_pstate != Train_cstate || Train_force_init_update ->
       c_code { y_init = y ; };
      :: else -> true
     fi;
     d_step {
      c_code {
       y_slope =  1 ;
       y_u = (y_slope * d) + y ;
       y_u = (y_u <  5.0  && signbit(y_slope) &&  y_init >  5.0)  ?  5.0 : y_u ;
       y_u = (y_u >  15.0  && !signbit(y_slope) &&  y_init <  15.0)  ?  15.0 : y_u ;
       
       Train_y = y ;
      };
      Train_pstate = Train_cstate;
      Train_cstate = Train_t2;
      Train_force_init_update = false;
      
      
      Train_tick = true;
     };
     do
      :: Train_tick -> true
      :: else -> goto  T2
     od;
     ::  (c_expr { (y == 15.0) }
           && 
          Train_cstate == Train_t2)  -> 
      d_step {
            c_code {  y_u = y ; };
            c_code {  Train_y = y ; };
            Train_pstate =  Train_cstate ;
            Train_cstate =  Train_t3 ;
            Train_force_init_update = false;
            Train_tick = true;
      };
      do
       :: Train_tick -> true
       :: else -> goto  T3
      od;

     :: else -> true
   fi;
T3:
   if
    :: (c_expr { ((y < 25.0) && (y >= 15.0)) }  &&  true  &&  Train_cstate == Train_t3) ->
     if
      :: Train_pstate != Train_cstate || Train_force_init_update ->
       c_code { y_init = y ; };
      :: else -> true
     fi;
     d_step {
      c_code {
       y_slope =  1 ;
       y_u = (y_slope * d) + y ;
       y_u = (y_u <  15.0  && signbit(y_slope) &&  y_init >  15.0)  ?  15.0 : y_u ;
       y_u = (y_u >  25.0  && !signbit(y_slope) &&  y_init <  25.0)  ?  25.0 : y_u ;
       
       Train_y = y ;
      };
      Train_pstate = Train_cstate;
      Train_cstate = Train_t3;
      Train_force_init_update = false;
      
      
      Train_tick = true;
     };
     do
      :: Train_tick -> true
      :: else -> goto  T3
     od;
     ::  (c_expr { (y == 25.0) }
           && 
          Train_cstate == Train_t3)  -> 
      d_step {
            c_code {  y_u = 0 ; };
            c_code {  Train_y = y ; };
            Train_pstate =  Train_cstate ;
            Train_cstate =  Train_t1 ;
            Train_force_init_update = false;
            Train_tick = true;
      };
      do
       :: Train_tick -> true
       :: else -> goto  T1
      od;

     :: else -> true
   fi;
}
c_decl {


};



c_decl {double  Controller_y ;};
c_track "&Controller_y" "sizeof(double)";
bool Controller_UP = 0 ;
bool Controller_DOWN = 0 ;

mtype { Controller_cloc1, Controller_cloc2, Controller_cloc3, Controller_cloc4 };
proctype Controller () {
Cloc1:
   if
    :: (c_expr { 1 && (Controller_y < 5.0) }  &&  true  &&  Controller_cstate == Controller_cloc1) ->
     if
      :: Controller_pstate != Controller_cstate || Controller_force_init_update ->
       c_code {  };
      :: else -> true
     fi;
     d_step {
      c_code {
       
       
       
       
      };
      Controller_pstate = Controller_cstate;
      Controller_cstate = Controller_cloc1;
      Controller_force_init_update = false;
      Controller_UP  = false;
      Controller_DOWN  = false;
      
      Controller_tick = true;
     };
     do
      :: Controller_tick -> true
      :: else -> goto  Cloc1
     od;
     ::  (c_expr { ((Controller_y >= 5.0) && (Controller_y < 15.0)) }
           && 
          Controller_cstate == Controller_cloc1)  -> 
      d_step {
            Controller_DOWN = 1;
            Controller_pstate =  Controller_cstate ;
            Controller_cstate =  Controller_cloc2 ;
            Controller_force_init_update = false;
            Controller_tick = true;
      };
      do
       :: Controller_tick -> true
       :: else -> goto  Cloc2
      od;

     :: else -> true
   fi;
Cloc2:
   if
    :: (c_expr { 1 && (Controller_y < 15.0) }  &&  true  &&  Controller_cstate == Controller_cloc2) ->
     if
      :: Controller_pstate != Controller_cstate || Controller_force_init_update ->
       c_code {  };
      :: else -> true
     fi;
     d_step {
      c_code {
       
       
       
       
      };
      Controller_pstate = Controller_cstate;
      Controller_cstate = Controller_cloc2;
      Controller_force_init_update = false;
      Controller_UP  = false;
      Controller_DOWN  = false;
      
      Controller_tick = true;
     };
     do
      :: Controller_tick -> true
      :: else -> goto  Cloc2
     od;
     ::  (c_expr { ((Controller_y >= 15.0) && (Controller_y < 25.0)) }
           && 
          Controller_cstate == Controller_cloc2)  -> 
      d_step {
            Controller_UP = 1;
            Controller_pstate =  Controller_cstate ;
            Controller_cstate =  Controller_cloc3 ;
            Controller_force_init_update = false;
            Controller_tick = true;
      };
      do
       :: Controller_tick -> true
       :: else -> goto  Cloc3
      od;

     :: else -> true
   fi;
Cloc3:
   if
    :: (c_expr { 1 && (Controller_y < 25.0) }  &&  true  &&  Controller_cstate == Controller_cloc3) ->
     if
      :: Controller_pstate != Controller_cstate || Controller_force_init_update ->
       c_code {  };
      :: else -> true
     fi;
     d_step {
      c_code {
       
       
       
       
      };
      Controller_pstate = Controller_cstate;
      Controller_cstate = Controller_cloc3;
      Controller_force_init_update = false;
      Controller_UP  = false;
      Controller_DOWN  = false;
      
      Controller_tick = true;
     };
     do
      :: Controller_tick -> true
      :: else -> goto  Cloc3
     od;
     ::  (c_expr { (Controller_y >= 25.0) }
           && 
          Controller_cstate == Controller_cloc3)  -> 
      d_step {
            
            Controller_pstate =  Controller_cstate ;
            Controller_cstate =  Controller_cloc4 ;
            Controller_force_init_update = false;
            Controller_tick = true;
      };
      do
       :: Controller_tick -> true
       :: else -> goto  Cloc4
      od;

     :: else -> true
   fi;
Cloc4:
   if
    :: (c_expr { 1 && (Controller_y >= 25.0) }  &&  true  &&  Controller_cstate == Controller_cloc4) ->
     if
      :: Controller_pstate != Controller_cstate || Controller_force_init_update ->
       c_code {  };
      :: else -> true
     fi;
     d_step {
      c_code {
       
       
       
       
      };
      Controller_pstate = Controller_cstate;
      Controller_cstate = Controller_cloc4;
      Controller_force_init_update = false;
      Controller_UP  = false;
      Controller_DOWN  = false;
      
      Controller_tick = true;
     };
     do
      :: Controller_tick -> true
      :: else -> goto  Cloc4
     od;
     ::  (c_expr { (Controller_y <= 0.0) }
           && 
          Controller_cstate == Controller_cloc4)  -> 
      d_step {
            
            Controller_pstate =  Controller_cstate ;
            Controller_cstate =  Controller_cloc1 ;
            Controller_force_init_update = false;
            Controller_tick = true;
      };
      do
       :: Controller_tick -> true
       :: else -> goto  Cloc1
      od;

     :: else -> true
   fi;
}
proctype main () {
  do
     ::  (Gate_tick && 
          Train_tick && 
          Controller_tick)  -> 
      d_step {
        c_code { x = x_u;
                 y = y_u; };
        c_code { now.train_pos = (int)Train_y;
                 now.gate_pos = (int)Gate_x; };
        c_code { Controller_y = Train_y; }
        Gate_DOWN = Controller_DOWN;
        Gate_UP = Controller_UP;
        Controller_tick = false;
        Train_tick = false;
        Gate_tick = false;
        
      }
     :: else -> true
  od
}
init {
  d_step {
    c_code {
    
    };
    c_code { now.train_pos = (int)Train_y;
             now.gate_pos = (int)Gate_x; };
    c_code { Controller_y = Train_y; }
    Gate_DOWN = Controller_DOWN;
    Gate_UP = Controller_UP;
    Gate_cstate = Gate_top;
    Train_cstate = Train_t1;
    Controller_cstate = Controller_cloc1;
  }
  atomic {
    run main();
    run Controller();
    run Train();
    run Gate();
    
  }
}