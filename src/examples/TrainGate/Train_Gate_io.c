#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<string.h>
#include<assert.h>

/* The files are in csv format */
#define OFILE "train_gate.csv"

#define TRUE 1
#define FALSE 0

extern double Gate_x, Train_t, Train_y, Controller_y;
extern unsigned char Controller_UP, Controller_DOWN;
unsigned char Gate_DOWN, Gate_UP;
/* extern double Train_signal; */
double signal_pre=0;



/* The step size */
double d = 0.03;

/* The events to print */
/* int DOWN=0, UP=0; */

/* The tick counter */
/* It is static to hide it inside this file */
static size_t tick = 0;

/* The output file pointer */
FILE *fo = NULL;

void readInput() {
  /* if (Train_signal != signal_pre){ */
  /*   if (Train_signal == 1){ */
  /*     DOWN=1; */
  /*     UP=0; */
  /*   } */
  /*   else{ */
  /*     DOWN=0; */
  /*     UP=1; */
  /*   } */
  /*   signal_pre=Train_signal; */
  /* } */
  /* else { */
  /*   DOWN=0; */
  /*   UP=0; */
  /* } */
}


/* Write output x to file */
void writeOutput(){
  static unsigned char count = 0;
  if (0 == count){
    fo = fopen(OFILE, "w");
    if (fo == NULL){
      perror(OFILE);
      exit(1);
    }
    ++count;
    fprintf(fo, "Time,Train_t,Train_y,Gate_x,Controller_y,Controller_UP,Controller_DOWN\n");
  }

  fprintf(fo, "%f,%f,%f,%f,%f,%d,%d\n", (++tick * d), Train_t, Train_y, Gate_x, Controller_y, Controller_UP, Controller_DOWN);

  if (tick==5000) {
    printf("Finished");
    exit(1);}
}


