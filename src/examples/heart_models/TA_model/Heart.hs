module Heart where
import Language
import qualified Hshac as H
import qualified Promela as P
import qualified Data.List as L
import Text.PrettyPrint.ANSI.Leijen
import GHC.IO.Handle
import System.IO
import Text.CSV
import Data.Word

(|>) :: a -> (a -> b) -> b
(|>) x f = f x

buildCell :: [String] -> Ha
buildCell cline = 
  let 
      name = (cline !! 0)
      neighbors =  read (cline !! 1) :: Word32
      tREST =  (read (cline !! 2) :: Integer) |> toRational
      tCOND =  (read (cline !! 3) :: Integer) |> toRational
      tREP =  (read (cline !! 4) :: Integer) |> toRational
      tRRP =  (read (cline !! 5) :: Integer) |> toRational

      -- The cont variable
      t = contVar name
      -- The locs
      locs = buildLocs t tREST tCOND tREP tRRP
      initialloc = head locs
      edges = buildEdges locs neighbors t tREST tCOND tREP tRRP
      extins = (case neighbors of
                   1 -> [InputEventDecl "ACTcell1"]
                   2 -> [InputEventDecl "ACTcell1", InputEventDecl "ACTcell2"]
                   _ -> error "More than 2 neighbors!")  
   in  Ha (L name) locs initialloc edges [SymbolDecl t 0] [] [] extins [OutputEventDecl "ACTpath" TFalse]
  where
       contVar :: String -> Symbol
       contVar name = S (name ++ "_t")
       
       buildEdges :: [Loc] -> Word32 -> Symbol -> Rational -> Rational -> Rational -> Rational -> [Edge]
       buildEdges locs n t trest tcond terp trrp = 
         let 
             rest_st_1 = Edge (locs !! 0) (locs !! 1) 
                         [InvariantEdge (T t Language.> TR trest)] ([t := 0], [], []) []
             rest_st_2 = Edge (locs !! 0) (locs !! 1) 
                                                    [] ([t := 0], [], []) [Event (InputEvent "ACTcell1")]
             st_up = Edge (locs !! 1) (locs !! 2)
                                              [InvariantEdge (T t Language.> TR tcond)]
                                              ([t := 0], [], [(:!) (OutputEvent "ACTpath")]) []
             up_rrp = Edge (locs !! 2) (locs !! 3)
                                               [InvariantEdge (T t Language.> TR terp)]
                                               ([t := 0], [], []) []
             rrp_rest = Edge (locs !! 3) (locs !! 0)
                                                  [InvariantEdge (T t Language.> TR trrp)]
                                                  ([t := 0], [], []) []

             rrp_st = Edge (locs !! 3) (locs !! 1)
                                                [] ([t := 0], [], []) [Event (InputEvent "ACTcell1")]

             ret = [rest_st_1, rest_st_2, st_up, up_rrp, rrp_rest, rrp_st]
         in case n of
              1 -> ret
              2 -> Edge (locs !! 0) (locs !! 1) [] ([t := 0], [], []) [Event (InputEvent "ACTcell2")] : ret
              _ -> error "More than 2 neighbors!"

       buildLocs :: Symbol -> Rational -> Rational -> Rational -> Rational -> [Loc]
       buildLocs t trest tcond terp trrp = 
         let 
             rest = Loc (L "REST") [Ode (DiffT t) 1 0] 
                                   [InvariantLoc (DiffT t) (T t Language.<= TR trest)] ([], [])

             st = Loc (L "ST") [Ode (DiffT t) 1 0] 
                               [InvariantLoc (DiffT t) (T t Language.<= TR tcond)] ([], [])

             erp = Loc (L "ERP") [Ode (DiffT t) 1 0] 
                                 [InvariantLoc (DiffT t) (T t Language.<= TR terp)] ([], [])

             rrp = Loc (L "RRP") [Ode (DiffT t) 1 0] 
                                 [InvariantLoc (DiffT t) (T t Language.<= TR trrp)] ([], [])

         in [rest, st, erp, rrp]
         
buildPath :: [String] -> Ha
buildPath cline = 
  let name = cline !! 0
      delay = (read (cline !! 1) :: Integer) |> toRational
      -- The cont var
      t = contVar name
      -- The locs
      locs = buildLocs t delay
      initialloc = locs !! 0
      -- edges
      edges = buildEdges t locs delay
  in Ha (L name) locs initialloc edges [SymbolDecl t 0] [] [] [InputEventDecl "ACTpath"] [OutputEventDecl "ACTcell" TFalse]
  where
       contVar :: String -> Symbol
       contVar name = S (name ++ "_t")
       
       buildEdges :: Symbol -> [Loc] -> Rational -> [Edge]
       buildEdges t locs delay = 
         let idle_act = Edge ((locs !! 0)) 
                             ((locs !! 1)) [] ([t := 0], [], []) [Event (InputEvent "ACTpath")]
             act_idle = Edge ((locs !! 1))
                             ((locs !! 0)) 
                             [InvariantEdge (T t Language.> TR delay)] 
                                         ([t := 0], [], [(:!) (OutputEvent "ACTcell")]) []
         in [idle_act, act_idle]

       buildLocs :: Symbol -> Rational -> [Loc]
       buildLocs t delay = 
         let ode_idle = Ode (DiffT t) 0 0
             ode_act = Ode (DiffT t) 1 0
             idle = Loc (L "IDLE") [ode_idle] [InvariantLoc (DiffT t) TTrue] ([], [])
             act = Loc (L "ACT") [ode_act] [InvariantLoc (DiffT t) (T t Language.<= TR delay)] ([], [])
         in [idle, act]

makeHaSeries          :: [Ha] -> HaSeries
makeHaSeries []       = error "Cannot make HaSeries"
makeHaSeries (x : []) = LastHa x
makeHaSeries (a : b)  = a ::: makeHaSeries b

writeHa :: (String, Doc) -> IO ()
writeHa (x, y) = do
                 handle <- openFile x WriteMode
                 hPutDoc handle y
                 hClose handle
                 
makeBinds :: Bind -> [Ha] -> [Ha] -> [String] -> BindHa
makeBinds b iin out cline = 
  let inha = case L.find (\(Ha (L x) _ _ _ _ _ _ _ _) -> x Prelude.== (cline !! 0)) iin of
              Just x -> x
              Nothing -> error $ "Cannot find the ha named: " ++ (cline !! 0) ++ "in in-ha list"
      outha = case L.find (\(Ha (L x) _ _ _ _ _ _ _ _) -> x Prelude.== (cline !! 1)) out of
               Just x -> x
               Nothing -> error $ "Cannot find the ha named: " ++ (cline !! 1) ++ "in out-ha list"
  in [(inha, (LastBind b), outha)]
  

main :: IO ()
main = do 
       csvcell <- parseCSVFromFile "cells.csv"
       csvpath <- parseCSVFromFile "paths.csv"
       actcell1 <- parseCSVFromFile "ACTcell1.csv"
       actcell2 <- parseCSVFromFile "ACTcell2.csv"
       actpath <- parseCSVFromFile "ACTpath.csv"
       let cells = case csvcell of
                        Left x -> error (show x)
                        Right x -> map buildCell x

           paths = case csvpath of
                     Left x -> error (show x)
                     Right x -> map buildPath x

           cp = cells ++ paths
           fileNames = map (\(Ha (L n) _ _ _ _ _ _ _ _) -> ("Generated/" ++ n ++ ".c")) cp
           
           actcelll1 = case actcell1 of
                         Left x -> error (show x)
                         Right x -> x

           actcelll2 = case actcell2 of
                         Left x -> error (show x)
                         Right x -> x
           actpath1 = case actpath of
                          Left x -> error (show x)
                          Right x -> x
           binds1 = map (makeBinds ((InputEvent "ACTcell1") :< (OutputEvent "ACTcell")) cells paths) actcelll1 
           binds2 = map (makeBinds ((InputEvent "ACTcell2") :< (OutputEvent "ACTcell")) cells paths) actcelll2 
           binds3 = map (makeBinds ((InputEvent "ACTpath") :< (OutputEvent "ACTpath")) paths cells) actpath1
           binds = (binds1 ++ binds2 ++ binds3) |> L.concat

       -- Now compile the HAs
       let (cellsc, others) = H.compileReactions (cp |> makeHaSeries) binds |> unzip
           cellsp = P.compileReactions (makeHaSeries cp) binds []
           cellsc_fNames = zip fileNames cellsc 
           (oo1, oo2) = others |> unzip
       mapM_ writeHa $ cellsc_fNames
       x <- openFile "Generated/Main.c" WriteMode
       hPutDoc x $ H.mkMain (makeHaSeries cp) binds oo1 oo2
       hClose x
       x <- openFile "Heart.pml" WriteMode
       hPutDoc x $ cellsp
       hClose x
