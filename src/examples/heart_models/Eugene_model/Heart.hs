module Heart where
import Language
import qualified Hshac as H
import qualified Promela as P
import qualified Data.List as L
import qualified Extra as E
import Text.PrettyPrint.ANSI.Leijen
import GHC.IO.Handle
import System.IO
import Text.CSV

(|>) :: a -> (a -> b) -> b
(|>) x f = f x

buildContVars          :: [String] -> String -> [Symbol]
buildContVars [] _     = []
buildContVars (x:xs) n = S (n ++ "_" ++ x) : buildContVars xs n
                                                                     
buildCell :: [String] -> Ha
buildCell cline = 
  let name = cline !! 0
      v_t = cline !! 1
      v_r = cline !! 2
      vo_max = cline !! 3
      v_n_r_max = cline !! 4
      auto_rate = cline !! 5
      beta_x = cline !! 6
      beta_y = cline !! 7
      beta_z = cline !! 8
      al1_x = cline !! 9
      al2_x = cline !! 10
      al3_x = cline !! 11
      al4_x = cline !! 12
      al1_y = cline !! 13
      al2_y = cline !! 14
      al3_y = cline !! 15
      al4_y = cline !! 16
      al1_z = cline !! 17
      al2_z = cline !! 18
      al3_z = cline !! 19
      al4_z = cline !! 20
      a_m = cline !! 21
      c_m = cline !! 22
      neighbors = cline !! 23
      gamma_0 = cline !! 24
      sigma_0 = cline !! 25
      dist_0 = cline !! 26
      gamma_1 = cline !! 27
      sigma_1 = cline !! 28
      dist_1 = cline !! 29
      gamma_2 = cline !! 30
      sigma_2 = cline !! 31
      dist_2 = cline !! 32
      gamma_3 = cline !! 33
      sigma_3 = cline !! 34
      dist_3 = cline !! 35
      gamma_4 = cline !! 36
      sigma_4 = cline !! 37
      dist_4 = cline !! 38
      
      -- TODO: declare the contvars
      contVars = buildContVars ["vx", "vy", "vz", "g", "v", "ft", "theta" ,"v_O"] name
      decContVars = map (\x -> SymbolDecl x 0) contVars
      locs = buildLocs contVars name v_t v_r 
             vo_max v_n_r_max auto_rate beta_x beta_y beta_z 
             al1_x al2_x al3_x al4_x al1_y al2_y al3_y al4_y 
             al1_z al2_z al3_z al4_z a_m c_m neighbors gamma_0 
             sigma_0 dist_0 gamma_1 sigma_1 dist_1 gamma_2 sigma_2 
             dist_2 gamma_3 sigma_3 dist_3 gamma_4 sigma_4 dist_4
             
      initLoc = locs !! 0

      edges = buildEdges contVars name v_t v_r 
             vo_max v_n_r_max auto_rate beta_x beta_y beta_z 
             al1_x al2_x al3_x al4_x al1_y al2_y al3_y al4_y 
             al1_z al2_z al3_z al4_z a_m c_m neighbors gamma_0 
             sigma_0 dist_0 gamma_1 sigma_1 dist_1 gamma_2 sigma_2 
             dist_2 gamma_3 sigma_3 dist_3 gamma_4 sigma_4 dist_4 locs

  in Ha (L name) locs initLoc edges decContVars
         (getInputs (gamma_0,sigma_0,dist_0) (gamma_1,sigma_1,dist_1)
         (gamma_2,sigma_2,dist_2) (gamma_3,sigma_3,dist_3)
         (gamma_4,sigma_4,dist_4)) [OutputVarDecl (S "voo") 0, OutputVarDecl (S "state") 0] 
         [] []

  where
       getInputs a b c d _ = 
         let ret = [InputVarDecl (S "v_i_0")]
             ret1 = case a of
                      ("", "", "") -> []
                      _ -> [InputVarDecl (S "v_i_1")]
                      
             ret2 = case b of
                      ("", "", "") -> []
                      _ -> [InputVarDecl (S "v_i_2")]
             ret3 = case c of
                      ("", "", "") -> []
                      _ -> [InputVarDecl (S "v_i_3")]
             ret4 = case d of
                      ("", "", "") -> []
                      _ -> [InputVarDecl (S "v_i_4")]

         in ret ++ ret1 ++ ret2 ++ ret3 ++ ret4

       buildg :: (String , String , String) ->
                 (String , String , String) ->
                 (String , String , String) ->
                 (String , String , String) ->
                 (String , String , String) -> 
                 Symbol -> ArithExpr Rational ->
                 String -> String  -> Update Symbol
       buildg (g1,s1,d1) (g2,s2,d2) (g3,s3,d3) 
              (g4,s4,d4) (g5,s5,d5) g v am cm = 
         let g11 = (read g1 :: Double) |> toRational
             s11 = (read s1 :: Double) |> toRational 
             d11 = (read d1 :: Double) |> toRational
             amm = (read am :: Double) |> toRational
             cmm = (read cm :: Double) |> toRational
             gf1 = (A (InputVar (S "v_i_0")) - v) * AR g11 * AR s11 / (AR amm * AR cmm * AR d11)

             gf2 = case (g2,s2,d2) of
                     ("","","") -> 0
                     _ -> (A (InputVar (S "v_i_1")) - v) * AR ((read g2 :: Double) |> toRational) 
                       * AR ((read s2 :: Double) |> toRational) 
                          / (AR amm * AR cmm * AR ((read d2 :: Double) |> toRational))
                          
             gf3 = case (g3,s3,d3) of
                     ("","","") -> 0
                     _ -> (A (InputVar (S "v_i_2")) - v) * AR ((read g3 :: Double) |> toRational) 
                       * AR ((read s3 :: Double) |> toRational) 
                          / (AR amm * AR cmm * AR ((read d3 :: Double) |> toRational))

             gf4 = case (g4,s4,d4) of
                     ("","","") -> 0
                     _ -> (A (InputVar (S "v_i_3")) - v) * AR ((read g4 :: Double) |> toRational) 
                       * AR ((read s4 :: Double) |> toRational) 
                          / (AR amm * AR cmm * AR ((read d4 :: Double) |> toRational)) 
         
             gf5 = case (g5,s5,d5) of
                     ("","","") -> 0
                     _ -> (A (InputVar (S "v_i_4")) - v) * AR ((read g5 :: Double) |> toRational) 
                       * AR ((read s5 :: Double) |> toRational) 
                          / (AR amm * AR cmm * AR ((read d5 :: Double) |> toRational))
                          
             gf = gf1 + gf2 + gf3 + gf4 + gf5

         in g := gf

       buildEdges contVars _ v_t v_r 
                  vo_max v_n_r_max _ beta_x beta_y beta_z 
                  al1_x al2_x al3_x al4_x al1_y al2_y al3_y al4_y 
                  al1_z al2_z al3_z al4_z a_m c_m _ gamma_0 
                  sigma_0 dist_0 gamma_1 sigma_1 dist_1 gamma_2 sigma_2 
                  dist_2 gamma_3 sigma_3 dist_3 gamma_4 sigma_4 dist_4
                  locs
         = let 
             x = contVars !! 0
             y = contVars !! 1
             z = contVars !! 2
             g = contVars !! 3
             v = contVars !! 4
             ft = contVars !! 5
             theta = contVars !! 6
             v_O = contVars !! 7

             gu = buildg (gamma_0,sigma_0,dist_0) (gamma_1,sigma_1,dist_1)
                         (gamma_2,sigma_2,dist_2) (gamma_3,sigma_3,dist_3)
                         (gamma_4,sigma_4,dist_4) g (A x - A y + A z) a_m c_m
             e1 = Edge (locs !! 0) (locs !! 1) [InvariantEdge (T g Language.> TR ((read v_t :: Double) |> toRational))]
                  ([x := (0.3 * A v), y := 0, z := (0.7 * A v), gu,
                    theta := (A v / AR ((read v_r :: Double) |> toRational)),
                    -- v := (A (S "vx_u") - A (S "vy_u") + A (S "vz_u")),
                    v_O := (AR ((read vo_max :: Double) |> toRational) - 
                            (80.1 * ((A v / AR ((read v_r :: Double) |> toRational)) Language.^ 0.5))),
                    ft := FCall "f" [A theta, AR ((read v_n_r_max :: Double) |> toRational)] (Just "double")], [], []) []
                    
             e2 = Edge (locs !! 1) (locs !! 0) [InvariantEdge (T g Language.<= TR ((read v_t :: Double) |> toRational)),
                                                InvariantEdge (T v Language.< TR ((read v_t :: Double) |> toRational))]
                  ([x := A x, y := A y, z := A z, gu
                    -- v := (A (S "vx_u") - A (S "vy_u") + A (S "vz_u"))
                   ], [], []) []
                    
             
             e3 = Edge (locs !! 1) (locs !! 2) [InvariantEdge (T v Language.>= TR ((read v_t :: Double) |> toRational))]
                  ([x := A x, y := A y, z := A z, gu
                    -- v := (A (S "vx_u") - A (S "vy_u") + A (S "vz_u"))
                   ], [], []) []

             -- XXX: This means there will be no dynamic behavior in the heart.
             e4 = Edge (locs !! 2) (locs !! 3) [InvariantEdge (T v Language.>= TR ((read vo_max :: Double) |> toRational))]
                  ([x := A x, y := A y, z := A z, gu
                    -- v := (A (S "vx_u") - A (S "vy_u") + A (S "vz_u"))
                   ], [], []) []
                    
             e5 = Edge (locs !! 3) (locs !! 0) [InvariantEdge (T v Language.<= TR ((read v_r :: Double) |> toRational))]
                  ([x := A x, y := A y, z := A z, gu
                    -- v := (A (S "vx_u") - A (S "vy_u") + A (S "vz_u"))
                   ], [], []) []

           in [e1, e2, e3, e4, e5]

       buildLocs contVars name v_t v_r 
             vo_max _ _ beta_x beta_y beta_z 
             al1_x al2_x al3_x al4_x al1_y al2_y al3_y al4_y 
             al1_z al2_z al3_z al4_z a_m c_m _ gamma_0 
             sigma_0 dist_0 gamma_1 sigma_1 dist_1 gamma_2 sigma_2 
             dist_2 gamma_3 sigma_3 dist_3 gamma_4 sigma_4 dist_4 = 
         let x = contVars !! 0
             y = contVars !! 1
             z = contVars !! 2
             g = contVars !! 3
             v = contVars !! 4
             ft = contVars !! 5
             a1x = (read al1_x :: Double) |> toRational
             a1y = (read al1_y :: Double) |> toRational
             a1z = (read al1_z :: Double) |> toRational
             t1_ode_x = Ode (DiffT x) (A x * AR a1x) 0
             t1_ode_y = Ode (DiffT y) (A y * AR a1y) 0
             t1_ode_z = Ode (DiffT z) (A z * AR a1z) 0
             vu_o = (OutputVar (S "voo")) := (A x - A y + A z)
             state_o = (OutputVar (S "state")) := 0
             vu = (v := (A x - A y + A z))
             gu = buildg (gamma_0,sigma_0,dist_0) (gamma_1,sigma_1,dist_1)
                         (gamma_2,sigma_2,dist_2) (gamma_3,sigma_3,dist_3)
                         (gamma_4,sigma_4,dist_4) g (A x - A y + A z) a_m c_m
             t1_inv = [InvariantLoc (DiffT v) (T v Language.<= TR ((read v_t :: Double) |> toRational)),
                       InvariantLoc (DiffT g) (T g Language.<= TR ((read v_t :: Double) |> toRational))]
             t1 = Loc (L (name ++ "_t1")) [t1_ode_x, t1_ode_y, t1_ode_z] t1_inv ([gu, vu], [vu_o, state_o])

             -- XXX: t2 
             a2x = (read al2_x :: Double) |> toRational
             a2y = (read al2_y :: Double) |> toRational
             a2z = (read al2_z :: Double) |> toRational
             b2x = (read beta_x :: Double) |> toRational
             b2y = (read beta_y :: Double) |> toRational
             b2z = (read beta_z :: Double) |> toRational
             t2_ode_x = Ode (DiffT x) ((A x * AR a2x) + (AR b2x * A g)) 0
             t2_ode_y = Ode (DiffT y) ((A y * AR a2y) + (AR b2y * A g)) 0
             t2_ode_z = Ode (DiffT z) ((A z * AR a2z) + (AR b2z * A g)) 0
             t2_inv = [InvariantLoc (DiffT v) (T v Language.< TR ((read v_t :: Double) |> toRational)),
                       InvariantLoc (DiffT g) (T g Language.> TR 0)]
             state_o2 = (OutputVar (S "state")) := 1
             t2 = Loc (L (name ++ "_t2")) [t2_ode_x, t2_ode_y, t2_ode_z] t2_inv ([gu, vu], [vu_o, state_o2])

             -- XXX: t3 
             a3x = (read al3_x :: Double) |> toRational
             a3y = (read al3_y :: Double) |> toRational
             a3z = (read al3_z :: Double) |> toRational
             t3_ode_x = Ode (DiffT x) (A x * AR a3x) ((read v_t :: Double) |> toRational)
             t3_ode_y = Ode (DiffT y) (A y * AR a3y) ((read v_t :: Double) |> toRational)
             t3_ode_z = Ode (DiffT z) (A z * AR a3z) ((read v_t :: Double) |> toRational)
             -- FIXME: vo_max should be symbol v_O
             t3_inv = [InvariantLoc (DiffT v) (T v Language.< TR ((read vo_max :: Double) |> toRational))]
             state_o3 = (OutputVar (S "state")) := 2
             t3 = Loc (L (name ++ "_t3")) [t3_ode_x, t3_ode_y, t3_ode_z] t3_inv ([gu, vu], [vu_o, state_o3])

              -- XXX: t4 
             a4x = (read al4_x :: Double) |> toRational
             a4y = (read al4_y :: Double) |> toRational
             a4z = (read al4_z :: Double) |> toRational
             t4_ode_x = Ode (DiffT x) (A x * AR a4x) ((read vo_max :: Double) |> toRational)
             t4_ode_y = Ode (DiffT y) (A y * AR a4y * A ft) ((read vo_max :: Double) |> toRational)
             t4_ode_z = Ode (DiffT z) (A z * AR a4z * A ft) ((read vo_max :: Double) |> toRational)
             t4_inv = [InvariantLoc (DiffT v) (T v Language.> TR ((read v_r :: Double) |> toRational))]
             state_o4 = (OutputVar (S "state")) := 3
             t4 = Loc (L (name ++ "_t4")) [t4_ode_x, t4_ode_y, t4_ode_z] t4_inv ([gu, vu], [vu_o, state_o4])
            in [t1, t2, t3, t4]

buildPath :: [String] -> Ha
buildPath cline = 
  let 
      name = cline !! 0
      c1t = (read (cline !! 1) :: Double) |> toRational
      c2t = (read (cline !! 2) :: Double) |> toRational
      c1ct = (read (cline !! 3) :: Double) |> toRational
      c2ct = (read (cline !! 4) :: Double) |> toRational
      ss = (read (cline !! 5) :: Double) |> toRational
      inst = (read (cline !! 6) :: Integer) |> toRational

      contVars = buildContVars ["k", "cell1_mode_delayed", "cell2_mode_delayed"
                               ,"from_cell" ,"cell1_replay_latch"
                               ,"cell2_replay_latch"
                               ,"cell1_v_delayed", "cell2_v_delayed"
                               , "wasted"] name
                               
      decContVars = map (\x -> 
                            SymbolDecl x
                             (case x of
                                 S n -> if n Prelude.== name ++ "_wasted"
                                        then FCall (name ++ "_init") [] Nothing
                                        else 0.0)) contVars
      locs = buildLocs contVars name inst
      initLoc = locs !! 0
      inputVars = [InputVarDecl (S "cell1_v"), InputVarDecl (S "cell1_mode"),
                   InputVarDecl (S "cell2_v"), InputVarDecl (S "cell2_mode")]
      outputVars = [OutputVarDecl (S "cell1_v_replay") 0,
                    OutputVarDecl (S "cell2_v_replay") 0]
      edges = buildEdges name contVars locs c1t c2t c1ct c2ct ss inst
  in Ha (L name) locs initLoc edges decContVars inputVars outputVars [] []
  where

  buildEdges :: String -> [Symbol] -> [Loc] -> Rational -> Rational 
                 -> Rational -> Rational -> Rational -> Rational 
                 -> [Edge]
  buildEdges hname contVars locs c1t c2t c1ct c2ct ss _ = 
    let 
        k = contVars !! 0
        c1md = contVars !! 1
        c2md = contVars !! 2
        fc = contVars !! 3
        c1rl = contVars !! 4
        c2rl = contVars !! 5
        c1vd = contVars !! 6
        c2vd = contVars !! 7
        wasted = contVars !! 8

        icell1_v = InputVar (S "cell1_v")
        icell2_v = InputVar (S "cell2_v") 
        icell1_m = InputVar (S "cell1_mode")
        icell2_m = InputVar (S "cell2_mode") 
        ocell1_v = OutputVar (S "cell1_v_replay")
        ocell2_v = OutputVar (S "cell2_v_replay")

        fcall1 = c1vd := FCall (hname ++ "_update_c1vd") [] (Just "double")
        fcall2 = c2vd := FCall (hname ++ "_update_c2vd") [] (Just "double")

        fcall3 = c2md := FCall (hname ++ "_update_c1md") [] (Just "double")
        fcall4 = c2md := FCall (hname ++ "_update_c2md") [] (Just "double")

        -- XXX: It will read icell1_v, etc automatically
        fcall5 = wasted := FCall (hname ++ "_update_buffer_index") 
                 [A icell1_v, A icell2_v, A icell1_m, A icell2_m] (Just "double")


        fcall6 = c1rl := FCall (hname ++ "_update_latch1") [A c1md, A (S (hname ++ "_cell1_replay_latch_u"))] 
                                                           (Just "double")
        fcall7 = c2rl := FCall (hname ++ "_update_latch2") [A c2md,  A (S (hname ++ "_cell2_replay_latch_u"))] 
                                                           (Just "double")
        fcalls = [fcall1, fcall2, fcall3, fcall4, fcall5, fcall6, fcall7]

        fcall8 = ocell1_v := FCall (hname ++ "_update_ocell1") [A (S (hname ++ "_cell1_v_delayed_u"))
                                                               , A (S (hname ++ "_cell1_replay_latch_u"))] (Just "double")
        fcall9 = ocell2_v := FCall (hname ++ "_update_ocell2") [A (S (hname ++ "_cell2_v_delayed_u"))
                                                               , A (S (hname ++ "_cell2_replay_latch_u"))] (Just "double")
        ofcalls = [fcall8 , fcall9]

        idle = locs !! 0
        ann = locs !! 1
        pd1 = locs !! 2
        pd2 = locs !! 3
        wc1 = locs !! 4
        wc2 = locs !! 5
        rc1 = locs !! 6
        rc2 = locs !! 7

        -- XXX: Edge 1 idle -- annihilate
        e1 = Edge idle ann [InvariantEdge ((T icell1_m Language.== TR 2) Language.&& (T icell2_m Language.== TR 2))]
             ([k := 1] ++ fcalls, ofcalls, []) []
        -- XXX: idle -- previous_direction1
        e2 = Edge  idle  pd1 [InvariantEdge ((T icell1_m Language.== TR 2) Language.&& (T icell2_m Language./= TR 2))]
             ([k := 1] ++ fcalls, ofcalls, []) []
        -- XXX: idle -- previous_direction2
        e3 = Edge  idle  pd2 [InvariantEdge ((T icell2_m Language.== TR 2) Language.&& (T icell1_m Language./= TR 2))]
             ([k := 1] ++ fcalls, ofcalls, []) []

        -- XXX: annihilate -- idle
        e4 = Edge  ann idle [InvariantEdge ((T icell1_m Language./= TR 2) Language.&& (T icell2_m Language./= TR 2))]
             ([k := 1, fc := 0] ++ fcalls, ofcalls, []) []

        -- XXX: pd1 -- annihilate
        e5 = Edge  pd1  ann [InvariantEdge ((T fc Language.== TR 2) Language.&& (T c2md Language./= TR 0))]
             ([k := 1] ++ fcalls, ofcalls, []) []
        -- XXX: pd1 -- wait_cell1 - 1
        e6 = Edge  pd1  wc1 [InvariantEdge ((T fc Language.== TR 2) Language.&& (T c2md Language.== TR 0))]
             ([k := 1] ++ fcalls, ofcalls, []) []
        -- XXX: pd1 -- wait_cell1 - 2
        e7 = Edge  pd1  wc1 [InvariantEdge (T fc Language.== TR 0)]
             ([k := 1] ++ fcalls, ofcalls, []) []
        -- XXX: pd1 -- wait_cell1 - 3
        e8 = Edge  pd1  wc1 [InvariantEdge (T fc Language.== TR 1)]
             ([k := 1] ++ fcalls, ofcalls, []) []

        -- XXX: wait_cell1 -- annihilate
        e17 = Edge  wc1  ann [InvariantEdge (T icell2_m Language.== TR 2)]
               ([k := 1] ++ fcalls, ofcalls, []) []
        -- XXX: wait_cell1 -- replay_cell1
        e9 = Edge  wc1  rc1 [InvariantEdge (T k Language.>= TR (c1t/ss))]
             ([fc := 1, c1rl := 1, k := 1] ++ fcalls, ofcalls, []) []

        -- XXX: replay_cell1 -- idle
        e10 = Edge  rc1  idle [InvariantEdge (T k Language.>= TR (c2ct/ss))]
               ([k := 1] ++ fcalls, ofcalls, []) []

        -- XXX: pd2 -- annihilate
        e14 = Edge pd2  ann [InvariantEdge ((T fc Language.== TR 1) Language.&& (T c1md Language./= TR 0))]
              ([k := 1] ++ fcalls, ofcalls, []) []
        -- XXX: pd2 -- wait_cell2 - 1
        e11 = Edge pd2  wc2 [InvariantEdge ((T fc Language.== TR 1) Language.&& (T c1md Language.== TR 0))]
               ([k := 1] ++ fcalls, ofcalls, []) []
        -- XXX: pd2 -- wait_cell2 - 2
        e12 = Edge pd2  wc2 [InvariantEdge (T fc Language.== TR 0)]
               ([k := 1] ++ fcalls, ofcalls, []) []
        -- XXX: pd2 -- wait_cell2 - 3
        e13 = Edge pd2 wc2 [InvariantEdge (T fc Language.== TR 2)]
               ([k := 1] ++ fcalls, ofcalls, []) []

        -- XXX: wait_cell2 -- annihilate
        e16 = Edge wc2 ann [InvariantEdge (T icell1_m Language.== TR 2)]
               ([k := 1] ++ fcalls, ofcalls, []) []
        -- XXX: wait_cell2 -- replay_cell2
        e15 = Edge wc2 rc2 [InvariantEdge (T k Language.>= TR (c2t/ss))]
               ([fc := 2, c2rl := 1, k := 1] ++ fcalls, ofcalls, []) []

        -- XXX: replay_cell2 -- idle
        e18 = Edge rc2 idle [InvariantEdge (T k Language.>= TR (c1ct/ss))]
               ([k := 1], ofcalls, []) []
    in [e1, e2, e3, e4, e5, e6, e7, e8, e9, e10
       ,e11, e12, e13, e14, e15, e16, e17, e18]

  buildLocs :: [Symbol] -> String -> Rational -> [Loc]
  buildLocs contVars name _ = 
    let 
        k = contVars !! 0
        c1md = contVars !! 1
        c2md = contVars !! 2
        c1vd = contVars !! 6
        c2vd = contVars !! 7
        wasted = contVars !! 8
        c1rl = contVars !! 4
        c2rl = contVars !! 5

        fcall1 = c1vd := FCall (name ++ "_update_c1vd") [] (Just "double")
        fcall2 = c2vd := FCall (name ++ "_update_c2vd") [] (Just "double")

        fcall3 = c1md := FCall (name ++ "_update_c1md") [] (Just "double")
        fcall4 = c2md := FCall (name ++ "_update_c2md") [] (Just "double")

        icell1_v = InputVar (S "cell1_v")
        icell2_v = InputVar (S "cell2_v") 
        icell1_m = InputVar (S "cell1_mode")
        icell2_m = InputVar (S "cell2_mode") 
        fcall5 = wasted := FCall (name ++ "_update_buffer_index") [A icell1_v, A icell2_v, A icell1_m, A icell2_m] 
                                                                  (Just "double")

        -- XXX: Cheating!
        fcall8 = c1rl := FCall (name ++ "_update_latch1") [A c1md, A (S (name ++ "_cell1_replay_latch_u"))] (Just "double")
        fcall9 = c2rl := FCall (name ++ "_update_latch2") [A c2md,  A (S (name ++ "_cell2_replay_latch_u"))] (Just "double")

        fcalls = [fcall1, fcall2, fcall3, fcall4, fcall5, fcall8, fcall9]
        ocell1_v = OutputVar (S "cell1_v_replay")
        ocell2_v = OutputVar (S "cell2_v_replay")
        fcall6 = ocell1_v := FCall (name ++ "_update_ocell1") [A (S (name ++ "_cell1_v_delayed_u"))
                                                              , A (S (name ++ "_cell1_replay_latch_u"))]
                                   (Just "double")
        fcall7 = ocell2_v := FCall (name ++ "_update_ocell2") [A (S (name ++ "_cell2_v_delayed_u"))
                                                               , A (S (name ++ "_cell2_replay_latch_u"))]
                                   (Just "double")
        -- fcall6 = ocell1_v := FCall (name ++ "_update_ocell1") [A c1vd, A (S (name ++ "_cell1_replay_latch_u"))]
        -- fcall7 = ocell2_v := FCall (name ++ "_update_ocell2") [A c2vd, A (S (name ++ "_cell2_replay_latch_u"))]
        ofcalls = [fcall6, fcall7]
        idle = Loc (L (name ++ "_idle")) [Ode (DiffT k) 1 0] 
                [InvariantLoc (DiffT k) TTrue]
                (fcalls, ofcalls)
        ann = Loc (L (name ++ "_annhilate")) [Ode (DiffT k) 1 1] 
               [InvariantLoc (DiffT k) TTrue]
               (fcalls, ofcalls)
        pd1 = Loc (L (name ++ "_previous_drection1")) [Ode (DiffT k) 1 1] 
               [InvariantLoc (DiffT k) TTrue]
               (fcalls, ofcalls)
        pd2 = Loc (L (name ++ "_previous_direction2")) [Ode (DiffT k) 1 1] 
               [InvariantLoc (DiffT k) TTrue]
               (fcalls, ofcalls)
        wc1 = Loc (L (name ++ "_wait_cell1")) [Ode (DiffT k) 1 1] 
              [InvariantLoc (DiffT k) TTrue]
              (fcalls, ofcalls)
        wc2 = Loc (L (name ++ "_wait_cell2")) [Ode (DiffT k) 1 1] 
              [InvariantLoc (DiffT k) TTrue]
              (fcalls, ofcalls)
        rc1 = Loc (L (name ++ "_replay_cell1")) [Ode (DiffT k) 1 1] 
              [InvariantLoc (DiffT k) TTrue]
              ([c1rl := 1] ++ fcalls, ofcalls)
        rc2 = Loc (L (name ++ "_replay_cell2")) [Ode (DiffT k) 1 1] 
              [InvariantLoc (DiffT k) TTrue]
              ([c2rl := 1] ++ fcalls, ofcalls)
     in [idle, ann, pd1, pd2, wc1, rc1, rc2, wc2]

makeBCells :: [Ha] -> [Ha] -> [String] -> BindHa
makeBCells iin out cline = 
  let inha = case L.find (\(Ha (L x) _ _ _ _ _ _ _ _) -> x Prelude.== (cline !! 0)) iin of
              Just x -> x
              Nothing -> error $ "Cannot find the ha named: " ++ (cline !! 0) ++ " in in-ha list"
      outha = case L.find (\(Ha (L x) _ _ _ _ _ _ _ _) -> x Prelude.== (cline !! 2)) out of
               Just x -> x
               Nothing -> if (cline !! 0) Prelude.== (cline !! 2)
                          then 
                              case L.find (\(Ha (L x) _ _ _ _ _ _ _ _) -> x Prelude.== (cline !! 0)) iin of
                                Just x -> x 
                                Nothing -> error $ "Cannot find the ha named: " ++ (cline !! 2) ++ " in out-ha list"
                          else error $ "Cannot find the ha named: " ++ (cline !! 2) ++ " in out-ha list"

      bind = (InputVar (S (cline !! 1)) :< (OutputVar (S (cline !! 3))))

  in [(inha, (LastBind bind), outha)]
  
makeBinds :: [Ha] -> [Ha] -> [String] -> BindHa
makeBinds iin out cline = 
  let inha = case L.find (\(Ha (L x) _ _ _ _ _ _ _ _) -> x Prelude.== (cline !! 0)) iin of
              Just x -> x
              Nothing -> error $ "Cannot find the ha named: " ++ (cline !! 0) ++ " in in-ha list"
      outha = case L.find (\(Ha (L x) _ _ _ _ _ _ _ _) -> x Prelude.== (cline !! 1)) out of
               Just x -> x
               Nothing -> error $ "Cannot find the ha named: " ++ (cline !! 1) ++ " in out-ha list"
               
      bind = case (cline !! 2) of
               "cell1_v" -> ((InputVar (S "cell1_v")) :< (OutputVar (S "voo")))
               "cell1_mode" -> ((InputVar (S "cell1_mode")) :< (OutputVar (S "state")))
               "cell2_v" -> ((InputVar (S "cell2_v")) :< (OutputVar (S "voo")))
               "cell2_mode" -> ((InputVar (S "cell2_mode")) :< (OutputVar (S "state")))
               _ -> error (show "Dont know what to do!")

  in [(inha, (LastBind bind), outha)]

writeHa :: (String, Doc) -> IO ()
writeHa (x, y) = do
                 handle <- openFile x WriteMode
                 hPutDoc handle y
                 hClose handle
makeHaSeries          :: [Ha] -> HaSeries
makeHaSeries []       = error "Cannot make HaSeries"
makeHaSeries (x : []) = LastHa x
makeHaSeries (a : b)  = a ::: makeHaSeries b
-- The main function
main :: IO ()
main = do 
       csvcell <- parseCSVFromFile "cells.csv"
       csvpath <- parseCSVFromFile "paths.csv"
       actcell <- parseCSVFromFile "cellMappings.csv"
       actpath <- parseCSVFromFile "pathMappings.csv"
       let cells = case csvcell of
                        Left x -> error (show x)
                        Right x -> map buildCell (L.tail x)

           paths = case csvpath of
                     Left x -> error (show x)
                     Right x -> map buildPath (L.tail x)
                     
           extras = E.buildExtra (case csvpath of
                                    Left x -> error (show x)
                                    Right x -> (L.tail x))
           pmaps = case actpath of
                        Left x -> error (show x)
                        Right x -> map (makeBinds paths cells) x

           cmaps = case actcell of
                     Left x -> error (show x)
                     Right x -> map (makeBCells cells paths) x

           binds = (pmaps |> L.concat) ++ (cmaps |> L.concat)

           cp = cells ++ paths
           fileNames = map (\(Ha (L n) _ _ _ _ _ _ _ _) -> ("Generated/" ++ n ++ ".c")) cp

       -- XXX: Writing the ha out into files.
       let (cellsc, others) = H.compileReactions (cp |> makeHaSeries) binds |> unzip
           (o1, o2) = unzip others
           cellsp = P.compileReactions (makeHaSeries cp) binds []
       mapM_ writeHa $ zip fileNames cellsc
       x <- openFile "Generated/Main.c" WriteMode
       hPutDoc x $ H.mkMain (makeHaSeries cp) binds o1 o2
       hClose x
       x1 <- openFile "Generated/Extra.c" WriteMode
       hPutDoc x1 $ extras |> vcat |> align
       hClose x1
       x2 <- openFile "Heart.pml" WriteMode
       hPutDoc x2 cellsp
       hClose x2
