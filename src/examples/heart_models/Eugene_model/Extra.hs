module Extra where
import qualified Data.List as L
import Prelude hiding ((<$>))
import Text.PrettyPrint.ANSI.Leijen
import System.IO
import Text.CSV

(|>) :: a -> (a -> b) -> b
(|>) x f = f x

build :: [String] -> Doc
build cline = 
  let name = cline !! 0
      c1ct = (read (cline !! 1) :: Double) 
      c2ct = (read (cline !! 2) :: Double)
      ss = (read (cline !! 5) :: Double)
      

      def1 = text $ "#define " ++ name ++ "_PC1NI ((int)" ++ (show (c1ct/ss)) ++ ")"
      def2 = text $ "#define " ++ name ++ "_PC2NI ((int)" ++ (show (c2ct/ss)) ++ ")"

      -- XXX: These are the variables we want.
      c1vb = text $ "static double* " ++ name ++ "_cell1_v_buffer;"
      c1mb = text $ "static unsigned char* " ++ name ++ "_cell1_mode_buffer;"
      c1ib = text $ "static unsigned int " ++ name ++ "_cell1_buffer_index;"
      c2vb = text $ "static double* " ++ name ++ "_cell2_v_buffer;"
      c2mb = text $ "static unsigned char* " ++ name ++ "_cell2_mode_buffer;"
      c2ib = text $ "static unsigned int " ++ name ++ "_cell2_buffer_index;"

      -- XXX: The init function
      i = text ("void " ++ name ++ "_init() {")
          <$> text (name ++ "_cell1_v_buffer = (double*)calloc(" ++ name ++ "_PC1NI, sizeof(double));") |> indent 2
          <$> text (name ++ "_cell1_mode_buffer = (unsigned char*)calloc(" ++ name ++ "_PC1NI, sizeof(unsigned char));") 
          |> indent 2
          <$> text (name ++ "_cell2_v_buffer = (double*)calloc(" ++ name ++"_PC2NI, sizeof(double));") |> indent 2
          <$> text (name ++ "_cell2_mode_buffer = (unsigned char*)calloc(" ++ name ++ "_PC2NI, sizeof(unsigned char));") 
          |> indent 2
          <$> text (name ++ "_cell1_buffer_index = 0;") |> indent 2
          <$> text (name ++ "_cell2_buffer_index = 0;") |> indent 2
          <$> text "}"

      j = text ("double " ++ name ++ "_update_c1vd() {")
          <$> text ("return " ++ name ++ "_cell1_v_buffer[" ++ name ++ "_cell1_buffer_index];")|> indent 2
          <$> text "}"
      k = text ("double " ++ name ++ "_update_c2vd() {")
          <$> text ("return " ++ name ++ "_cell2_v_buffer[" ++ name ++ "_cell2_buffer_index];") |> indent 2
          <$> text "}"
      l = text ("double " ++ name ++ "_update_c1md() {")
          <$> text ("return " ++ name ++ "_cell1_mode_buffer[" ++ name ++ "_cell1_buffer_index];")|> indent 2
          <$> text "}"
      m = text ("double " ++ name ++ "_update_c2md() {")
          <$> text ("return " ++ name ++ "_cell2_mode_buffer[" ++ name ++ "_cell2_buffer_index];")|> indent 2
          <$> text "}"
      n = text ("double " ++ name ++ "_update_buffer_index(double c1v, double c2v, double c1m, double c2m) {")
          <$> text (name ++ "_cell1_v_buffer["++ name ++"_cell1_buffer_index] = c1v;") |> indent 2
          <$> text (name ++ "_cell2_v_buffer["++ name ++"_cell2_buffer_index] = c2v;") |> indent 2
          <$> text (name ++ "_cell1_mode_buffer["++ name ++"_cell1_buffer_index] = c1m;") |> indent 2
          <$> text (name ++ "_cell2_mode_buffer["++ name ++"_cell2_buffer_index] = c2m;") |> indent 2
          <$> text (name ++ "_cell1_buffer_index = ((" ++ name ++ "_cell1_buffer_index + 1) >= " 
                    ++ name ++ "_PC1NI) ? 0 : ++" ++ name ++ "_cell1_buffer_index;")|> indent 2
          <$> text (name ++ "_cell2_buffer_index = ((" ++ name ++ "_cell2_buffer_index + 1) >= " 
                    ++ name ++ "_PC2NI) ? 0 : ++" ++ name ++ "_cell2_buffer_index;")|> indent 2
                    
          <$> text "return 0.0;" |> indent 2
          <$> text "}"
      o = text ("double " ++ name ++ "_update_ocell1(double c1vd, double c1lu) {")
          <$> text "return ((c1lu == 1) ? c1vd : 0);" |> indent 2
          <$> text "}"
      p = text ("double " ++ name ++ "_update_ocell2(double c2vd, double c2lu) {")
          <$> text "return ((c2lu == 1) ? c2vd : 0);" |> indent 2
          <$> text "}"
      q = text ("double " ++ name ++ "_update_latch1(double c1md, double c1lu) {")
          <$> text "return ((c1md == 0) ? 0 : c1lu);"|> indent 2
          <$> text "}"
      r = text ("double " ++ name ++ "_update_latch2(double c2md, double c2lu) {")
          <$> text "return ((c2md == 0) ? 0 : c2lu);"|> indent 2
          <$> text "}"


  in text "#include<stdlib.h>"
     <$> def1
     <$> def2
     <$> c1vb
     <$> c2vb
     <$> c1mb
     <$> c2mb
     <$> c1ib <$> c2ib
     <$> i <$> j <$> k <$> l <$> m <$> n
     <$> o <$> p <$> q <$> r 
     <$> empty <$> empty


buildExtra :: [[String]] -> [Doc]
buildExtra = map build

