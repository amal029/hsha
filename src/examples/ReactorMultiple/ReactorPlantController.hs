module ReactorPlantController where
import Language
import qualified Hshac as H
import Prelude hiding ((==),(/=), LT, GT, (<=), (>=), (<), (>), (&&), not)
import Text.PrettyPrint.ANSI.Leijen
import GHC.IO.Handle
import System.IO

dxdt :: Diff
dxdt = DiffT (S "x")

o0 :: Ode
o0 = Ode dxdt (0.1*A (S "x")+50) 510

o1 :: Ode
o1 = Ode dxdt (0.1*A (S ("x"))+56) 550

o2 :: Ode
o2 = Ode dxdt (0.1*A (S ("x"))+60) 550

t0 :: Loc
t0 = Loc (L "t0") [o0] [InvariantLoc dxdt TTrue] 
                       [OutputVar (S "x") := A (S "x")]

t1 :: Loc
t1 = Loc (L "t1") [o1] [InvariantLoc dxdt TTrue] 
                       [OutputVar (S "x") := A (S "x")]

t2 :: Loc
t2 = Loc (L "t2") [o2] [InvariantLoc dxdt TTrue] 
                       [OutputVar (S "x") := A (S "x")]
  
e1 :: Edge
e1 = Edge t0 t1 [InvariantEdge (T (S "x") >= TR 550)] 
                ([(S "x") := A (S "x")]
                ,[(OutputVar (S "x")) := A (S "x")]
                ,[]) 
                [Event (InternalEvent "add1")]
                
e2 :: Edge
e2 = Edge t1 t0 [InvariantEdge (T (S "x") <= TR 550)] 
                ([(S "x") := A (S "x")]
                ,[(OutputVar (S "x")) := A (S "x")]
                ,[])
                [Event (InternalEvent "remove1")]

e3 :: Edge
e3 = Edge t0 t2 [InvariantEdge (T (S "x") >= TR 550)] 
                ([(S "x") := A (S "x")]
                ,[(OutputVar (S "x")) := A (S "x")]
                ,[])
                [Event (InternalEvent "add2")]
                
e4 :: Edge
e4 = Edge t2 t0 [InvariantEdge (T (S "x") <= TR 550)] 
                ([(S "x") := A (S "x")]
                ,[(OutputVar (S "x")) := A (S "x")]
                ,[])
                [Event (InternalEvent "remove2")]
                
reactorPlant :: Ha
reactorPlant = Ha (L "ReactorPlant") [t0, t1, t2] t0 [e1, e2, e3, e4]
               [S ("x")] [] [OutputVarDecl (S "x") 510] [] []


-- TODO: Need to write the controller, bind, and update the main
-- function.
   
dty1dt :: Diff
dty1dt = DiffT (S "ty1")

dty2dt :: Diff
dty2dt = DiffT (S "ty2")

odety1 :: Ode
odety1 = Ode dty1dt (1) 10

odety2 :: Ode
odety2 = Ode dty2dt (1) 10

tc0 :: Loc
tc0 = Loc (L "tc0") [odety2, odety1] [InvariantLoc dxdt (T (InputVar (S "x")) < TR 550),
                                      InvariantLoc dty1dt (T (S "ty1") <= TR 100000),
                                      InvariantLoc dty2dt (T (S "ty2") <= TR 100000)]
          []
          
tc1 :: Loc
tc1 =  Loc (L "tc1") [odety2, odety1] [InvariantLoc dxdt (T (InputVar (S "x")) > TR 510),
                                      InvariantLoc dty1dt (T (S "ty1") <= TR 100000),
                                      InvariantLoc dty2dt (T (S "ty2") <= TR 100000)]
           []

tc2 :: Loc
tc2 = Loc (L "tc2") [odety2, odety1] [InvariantLoc dxdt (T (InputVar (S "x")) > TR 510),
                                      InvariantLoc dty1dt (T (S "ty1") <= TR 100000),
                                      InvariantLoc dty2dt (T (S "ty2") <= TR 100000)]
          []
          
-- TODO: Put the edges in next

main :: IO ()
main = do 
       x <- openFile "ReactorPlant.c" WriteMode
       hPutDoc x $ H.compileReaction reactorPlant
       y <- openFile "Main.c" WriteMode
       hPutDoc y $ H.mkMain (LastHa reactorPlant) []
       hClose x
       hClose y
