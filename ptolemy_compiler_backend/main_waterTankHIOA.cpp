#include <iostream>
#include "waterTankControllerHIOA.hpp"

#define T 20.0			// The total time to run the simulation
				// from Ptolemy
#define F "output.csv"		// The file to plot the output to

using namespace std;

// The delta
double delta = 0.1; 		// This is the step size
size_t k = 0;			// This is the step number
double time_delay = 1.2;	// This is the feedback delay from
				// Ptolemy


int main()
{
  FILE *f = fopen(F, "w");
  while (k < (T/delta)) {
    // Print the outputs from different states.
    if (waterTank_cstate == waterTank_Mode4)
      fprintf(f, "%g %g\n", (k*delta), waterTank_myActor4_I.OUTPUT.Mode4_Y[0]);
    else if (waterTank_cstate == waterTank_Mode)
      fprintf(f, "%g %g\n", (k*delta), waterTank_myActor_I.OUTPUT.Mode_Y[0]);
    else if (waterTank_cstate == waterTank_Mode3)
      fprintf(f, "%g %g\n", (k*delta), waterTank_myActor3_I.OUTPUT.Mode3_Y[0]);
    else if (waterTank_cstate == waterTank_Mode2)
      fprintf(f, "%g %g\n", (k*delta), waterTank_myActor2_I.OUTPUT.Mode2_Y[0]);

    // Called the watertank model
    waterTank_cstate = waterTank(waterTank_cstate);
    // Called the controller
    controller_cstate = controller(controller_cstate);
    // Now call the connections
    connections();
    // Increment the step
    ++k;

  }

  fflush(f);
  fclose(f);
  f = popen("gnuplot -persistent", "w");
  fprintf(f, "plot '%s' using 1:2 with points title 'Output'\n", F);
  pclose(f);
  return 0;
}

