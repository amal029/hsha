#!/usr/bin/env python3

import abc
import ExpressionParser
import Port


class Entity():

    """This is the abstract base class for all Entities code-generation

    """

    def __init__(self, kwargs):
        self.name = None
        self.inputs = []
        self.outputs = []
        self.relations = []
        self.body = ''
        try:
            self.name = kwargs['name']
        except Exception as e:
            raise RuntimeError('Name of entity not provided')
        try:
            self.inputs = kwargs['inputs']
        except Exception:
            pass
        try:
            self.outputs = kwargs['outputs']
        except Exception:
            pass
        try:
            self.relations = kwargs['relations']
        except Exception:
            pass
        try:
            self.body = kwargs['body']
        except Exception:
            pass

    @abc.abstractmethod
    def codeGen(self):
        raise NotImplementedError('Should be implemented by the Subclasses')

    @abc.abstractstaticmethod
    def parse(node, **args):
        raise NotImplementedError('Should be implemented by the Subclasses')

    @staticmethod
    def getIO(node):
        ins, outs = [], []
        # Get the inputs
        for i in node.iterfind('.//port[@name="input"]'):
            ins += [Port.Port.parse(i)]
        for o in node.iterfind('.//port[@name="output"]'):
            outs += [Port.Port.parse(o)]
        return (ins, outs)

    @staticmethod
    def getRowsColumns(matrix):
        # Strip the brackets
        matrix = matrix.strip('[').strip(']')
        return [r.split(',') for r in matrix.split(';')]


class ModalModel(Entity):
    @staticmethod
    def getDestState(links, rel):
        links = list(links)

        # Now for the relation specified get the destination port name
        ports = [p.get('port').split('.')[0]
                 for p in links
                 if p.get('relation') == rel
                 and p.get('port').split('.')[1] == 'incomingPort']
        return ports[0] if len(ports) == 1 else None

    # XXX: This should be moved into codeGen later on
    @staticmethod
    def parse(model, to_bind_input_values, root):
        hs, bs = '', ''
        modes = list(model.iterfind(
            "./entity[@name='_Controller']"
            "/entity[@class='ptolemy.domains.modal.kernel.ha.Mode']"))
        # Now get all the modes in the modal model
        mode_io = dict()
        for mode in modes:
            headers, bodies, refs = Mode.parse(mode, to_bind_input_values,
                                               model)
            hs += '\n'.join(headers)
            bs += '\n'.join(bodies)
            mode_io[mode.get('name')] = refs

        # Get all the modalports
        allports = model.findall('.//port[@class='
                                 '"ptolemy.domains.modal.modal.ModalPort"]')
        # Automatically declared ports in HIOA
        modeios = [i.name for r in mode_io.values()
                   for i in r[0].inputs]
        modeios += [i.name for r in mode_io.values()
                    for i in r[0].outputs]
        userports = [p for p in allports
                     if p.get('name') not in modeios]
        upi = [p for p in userports
               if p.find('.//property[@name="input"]') is not None]
        upo = [p for p in userports
               if p.find('.//property[@name="output"]') is not None]
        model_io = {model.get('name'): [upi, upo]}
        parser = ExpressionParser.Parser(mode_io, model_io)
        event_parser = ExpressionParser.Parser(mode_io, model_io, event=True)

        # Get the transition guards and actions
        relga = {}
        for t in model.iterfind(
                ('.//relation[@class="ptolemy.domains.modal.'
                 'kernel.Transition"]')):
            guards = list(t.iterfind('.//property[@name="guardExpression"]'))
            actions = list(t.iterfind('.//property[@name="setActions"]'))
            oactions = list(t.iterfind('.//property[@name="outputActions"]'))

            toadd = dict()
            toadd['g'] = guards[0].get('value') if guards != [] else None
            toadd['a'] = actions[0].get('value') if actions != [] else None
            toadd['oa'] = oactions[0].get('value') if oactions != [] else None

            relga[t.get('name')] = toadd  # [guards, actions, oactions]

        # Get all the outgoing transitions from each mode
        modega = {}
        mode_o_i = {}
        model_controller = list(model.iterfind(
            './/entity[@class="ptolemy.domains.'
            'modal.modal.ModalController"]'))[0]
        links = list(model_controller.iterfind('link'))
        links = [l for l in links
                 if l.get('relation') in relga.keys()]
        for m in modes:
            connm = []
            for l in links:
                mn, mrt = (l.get('port').split('.')[0],
                           l.get('port').split('.')[1])
                if (m.get('name') == mn and mrt == 'outgoingPort'):
                    connm += [l.get('relation')]
            modega[m.get('name')] = [relga[n] for n in connm]
            mode_o_i[m.get('name')] = [ModalModel.getDestState(links, c)
                                       for c in connm]
        # Build the initial state and function
        initial_modes = [m
                         for m in modes
                         if list(m.iterfind(
                                 './/property[@name="isInitialState"]')) != []]
        initial_modes = [m.get('name') for m in initial_modes
                         if m.find('.//property[@name="isInitialState"]').
                         get('value') == "true"]
        # Now build the switch statement for the modal model
        # Declare the enum states of this ModalModel
        header, body = [], []
        header.append('enum '+model.get('name')+'_states {' +
                      ', '.join([model.get('name')+'_'+m.get('name')
                                 for m in modes])+'};')
        for im in initial_modes:
            # This is defined in the header file
            header.append('extern enum '+model.get('name')+'_states '
                          + model.get('name')+'_cstate;')
            # This is defined in the "C" file
            body.append('enum '+model.get('name')+'_states ' +
                        model.get('name')+'_cstate = '+model.get('name')+'_' +
                        im+';')

        header.append('enum '+model.get('name')+'_states '
                      + model.get('name') + '(enum '+model.get('name') +
                      '_states);')

        # Add the new structure for the modal model here
        # This will include all the user defined ports

        # Declare the struct
        header.append('typedef struct {')
        # XXX: Only single dimensional inputs and outputs of type double
        # are supported for now.

        # Append the inputs
        header.append('struct {')
        for p in upi:
            header.append('double '+p.get('name')+'[1];')
        header.append('} INPUT;')
        # Append the outputs
        header.append('struct {')
        for p in upo:
            header.append('double '+p.get('name')+'[1];')
            header.append('unsigned char '+p.get('name')+'_event[1];')
        header.append('} OUTPUT;')
        header.append('} T_'+model.get('name')+';')
        header.append('extern T_'+model.get('name')+' '
                      + model.get('name')+'_I;')

        # Initialize the toplevel
        body.append('T_'+model.get('name')+' '+model.get('name')+'_I;')
        body.append('enum '+model.get('name')+'_states '
                    + model.get('name') +
                    ' (enum '+model.get('name')+'_states cstate) {')
        # Now here goes the state machine
        body.append('switch (cstate) {')
        for m in modes:
            body.append('case ('+model.get('name')+'_'+m.get('name')+'):')
            body.append('if (0) {;}')
            # put the outgoing transitions first
            for g, rm in zip(modega[m.get('name')], mode_o_i[m.get('name')]):
                body.append('else if (1 &&')
                body.append('&& '.join(['!'+model.get('name')+'_' +
                                        r.name+'_I._FT'
                                        for r in mode_io[m.get('name')]]))
                body.append(' && ' + (parser.input_parse(g['g'])
                            if g['g'] is not None else '1') + ') {')
                # FIXME: This cstate is being assigned the wrong
                # destination state!!
                body.append('cstate = '+model.get('name')+'_'+rm+';')
                for r in mode_io[m.get('name')]:
                    body.append(model.get('name')+'_'+r.name+'_I._FT = 1;')
                body.append((parser.input_parse(g['a'])
                            if g['a'] is not None else '') + ';')
                body.append((parser.input_parse(g['oa'])
                            if g['oa'] is not None else '') + ';')
                # Adding the event for user defined I/O
                body.append((event_parser.input_parse(g['oa'])
                            if g['oa'] is not None else '') + ';')
                body.append('}')
            body.append('else {')
            # Call all your refinements here
            for r in mode_io[m.get('name')]:
                body.append(model.get('name')+'_'+r.name+'();')
            body.append('}')
            body.append('break;')
        body.append('default:')
        body.append('fprintf(stderr, "Reached unreachable state!\\n");')
        body.append('exit(1);')
        body.append('}')
        body.append('return cstate;')
        body.append('}')

        # Add the header
        hs += '\n'.join(header)
        bs += '\n'.join(body)

        return hs, bs, model_io, {model.get('name'): mode_io}


class Mode(Entity):
    def __init__(self, mode, kwargs):
        pass
        # TODO: 1.) Get the parameters of this mode

    @staticmethod
    def parse(mode, to_bind_input_values, root):
        # The system matrix size
        A = list(mode.iterfind('.//property[@name="A"]'))[0].get('value')
        # The input matrix
        B = list(mode.iterfind('.//property[@name="B"]'))[0].get('value')
        # The output matrix
        C = list(mode.iterfind('.//property[@name="C"]'))[0].get('value')
        # The U affecting Y matrix
        D = list(mode.iterfind('.//property[@name="D"]'))[0].get('value')
        # The initial state matrix
        IS = list(mode.iterfind('.//property[@name="I"]'))[0].get('value')

        A = Entity.getRowsColumns(A)
        B = Entity.getRowsColumns(B)
        C = Entity.getRowsColumns(C)
        D = Entity.getRowsColumns(D)
        IS = Entity.getRowsColumns(IS)

        # Now get the modes and parse those
        modes = dict()
        for ref in mode.iterfind('.//*[@name="refinementName"]'):
            # Get all the refinements indexed by modes
            modes[mode.get('name')] = root.iterfind(
                './/entity[@name="'+ref.get('value')+'"]')
        headers = []
        bodies = []
        refs = []
        for mo in modes.keys():
            for uu in Refinement.parse(modes[mo], A, B, C, D, IS,
                                       to_bind_input_values):
                h, b = uu.codeGen(root.get('name'))
                headers += [h]
                bodies += [b]
                refs += [uu]
        return headers, bodies, refs


class Refinement(Entity):
    def __init__(self, kwargs):
        super().__init__(kwargs)
        self.A = kwargs['A']
        self.B = kwargs['B']
        self.C = kwargs['C']
        self.D = kwargs['D']
        self.IS = kwargs['IS']
        self.s = kwargs['s']
        self.u = kwargs['u']
        self.ui = kwargs['ui']
        self.y = kwargs['y']

    def codeGen(self, model_name):
        # First get a struct from inputs
        ibuf = 'struct{\n'
        ibuf += '\n'.join([i.codeGen() for i in self.inputs])
        ibuf += '\n}INPUT;\n'
        obuf = 'struct{\n'
        obuf += '\n'.join([o.codeGen() for o in self.outputs])
        obuf += '\n}OUTPUT;\n'
        body = 'void ' + model_name + '_' + self.name + '() {\n'
        # Encode the initial state
        body += 'if('+model_name+"_"+self.name+'_I._FT) {\n'
        for i in range(len(self.A)):
            body += (model_name+'_'+self.name+'_I.OUTPUT.'+self.s[0].name+'[' +
                     str(i)+']')
            body += ' = '
            body += model_name+'_'+self.name+'_I._IS['+str(i)+'];\n'
        body += model_name+'_'+self.name+'_I._FT = 0;\n'
        body += '}\n'
        body += 'double slope_' + self.name + ' = 0;\n'
        for i in range(len(self.A)):
            body += ('slope_'+self.name+" = 0;\n")
            for j in range(len(self.A[i])):
                body += ('slope_'+self.name + ' += '
                         + model_name+'_'+self.name + '_I.OUTPUT.'
                         + self.s[0].name+'['+str(j)+'] * '
                         + self.A[i][j].strip() + ';\n')
            # Adding the inputs
            for j in range(len(self.B[i])):
                body += ('slope_'+self.name + ' += '
                         + model_name+'_'+self.name + '_I.INPUT.'
                         + self.u[0].name+'['+str(j)+'] * '
                         + self.B[i][j].strip() + ';\n')

            # Now do the actual integration step
            body += '//Forward Euler\n'
            body += (model_name+'_'+self.name+'_I.OUTPUT.'+self.s[0].name
                     + '['+str(i) + '] = delta * slope_' +
                     self.name + ' + '+model_name+'_'+self.name+'_I.OUTPUT.'
                     + self.s[0].name + '[' + str(i) + ']'
                     + ';\n')
        # Now we can do the output too.
        body += '//Giving outputs\n'
        for i in range(len(self.C)):
            body += 'slope_' + self.name + ' = 0;\n'
            for j in range(len(self.C[i])):
                body += ('slope_'+self.name + ' += '
                         + model_name+'_'+self.name + '_I.OUTPUT.'
                         + self.s[0].name+'['+str(j)+'] * '
                         + self.C[i][j].strip() + ';\n')
            # Adding the inputs
            for j in range(len(self.D[i])):
                body += ('slope_'+self.name + ' += '
                         + model_name+'_'+self.name + '_I.INPUT.'
                         + self.u[0].name+'['+str(j)+'] * '
                         + self.D[i][j].strip() + ';\n')

            # Now do the actual integration step
            body += (model_name+'_'+self.name+'_I.OUTPUT.'+self.y[0].name
                     + '['+str(i) + '] = slope_' +
                     self.name + ';\n')
            body += (model_name+'_'+self.name+'_I.OUTPUT.'+self.y[0].name
                     + '_event['+str(i)+'] = 1;\n')
        body += '\n}\n'

        # IO
        io = 'typedef struct ' + model_name+'_'+self.name + '{\n'
        # Make the matrix to hold the initial state.
        io += 'double _IS ['+str(len(self.IS[0]))+'];\n'
        io += 'unsigned char _FT;\n'
        io += ibuf
        io += obuf
        io += '} T_'+model_name+'_'+self.name+';\n'
        # Initialize the initial state vector of the struct instance
        vals = ','.join([i.strip() for i in self.IS[0]])
        # Also make an instance of these types
        io += ('extern T_' + model_name+'_'+self.name + ' ' + model_name +
               '_' + self.name +
               '_I;\n')
        # Designated initializers do not work with g++ but work with clang++!
        # body += ('T_' + model_name + "_" + self.name + ' ' + model_name + '_'
        #          + self.name +
        #          '_I = {._IS = {'+vals+'}, ._FT = 1, ' +
        #          '.INPUT.' + self.inputs[0].name + ' = {' +
        #          ','.join(self.ui) +
        #          '}' + '};\n')
        body += ('T_' + model_name + "_" + self.name + ' ' + model_name + '_'
                 + self.name +
                 '_I = {{'+vals+'}, 1, ' +
                 '{' +
                 ','.join(self.ui) +
                 '}' + '};\n')
        return io,  body

    @staticmethod
    def getDims(links, LTI):
        links = list(links)
        LTI = list(LTI)[0].get('name')

        # first get relations of LTI connections
        urels = [r.get('relation') for r in links
                 if r.get('port') == LTI+'.input']
        yrels = [r.get('relation') for r in links
                 if r.get('port') == LTI+'.output']
        srels = [r.get('relation') for r in links
                 if r.get('port') == LTI+'.stateOutput']

        # Now for each of these relations get the port name
        uports = [p.get('port') for p in links
                  if p.get('relation') in urels]
        yports = [p.get('port') for p in links
                  if p.get('relation') in yrels]
        sports = [p.get('port') for p in links
                  if p.get('relation') in srels]

        return uports, yports, sports

    @staticmethod
    def parse(nodes, A, B, C, D, IS, to_bind_input_values):
        for n in nodes:
            kw = {'name': n.get('name')}
            # Get the connections from relations.
            usedports = [l.get('port') for l in n.iterfind('link')]

            # Get the connections
            u, y, s = Refinement.getDims(
                n.iterfind('link'),
                n.iterfind('.//entity[@class="ptolemy.domains.'
                           'continuous.lib.LinearStateSpace"]'))

            iports = [p if p.get('name') in usedports else None
                      for p in n.iterfind('port')
                      if list(p.iterfind('./property[@name="input"]')) != []]
            oports = [p if p.get('name') in usedports else None
                      for p in n.iterfind('port')
                      if list(p.iterfind('./property[@name="output"]')) != []]
            # Filter the unsed ones
            kw['inputs'] = [Port.Port.parse(p, isInput=True,
                                            dim=len(B[0]))
                            for p in iports if p is not None]
            kw['outputs'] = [Port.Port.parse(p, isInput=False,
                                             dim=len(C)
                                             if p.get('name') in y else len(A))
                             for p in oports if p is not None]
            kw['A'] = A
            kw['B'] = B
            kw['C'] = C
            kw['D'] = D
            kw['IS'] = IS
            kw['s'] = [p for p in kw['outputs']
                       if p.name in s]
            kw['y'] = [p for p in kw['outputs']
                       if p.name in y]
            kw['u'] = [p for p in kw['inputs']
                       if p.name in u]
            kw['ui'] = [v
                        for i in kw['u']
                        for v in to_bind_input_values[i.name]]

            yield Refinement(kw)


class Expression(Entity):
    pass


class TimedPlotter(Entity):
    pass


class ContinuousMerge(Entity):
    pass
