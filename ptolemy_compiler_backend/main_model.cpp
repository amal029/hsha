#include "ModalModel.hpp"

#define T 20.0 			/* This comes from the model in Ptolemy */
double delta = 0.001; 		/* I have given this */
#define F "output.csv"

int main(void)
{
  /* The output file */
  FILE *f1 = fopen(F, "w");
  /* Just call the modalmodel in a loop */
  int counter = 0;
  while (counter < (T/delta)) {
    if (ModalModel_cstate == ModalModel_Mode){
      fprintf(f1, "%g %g\n", (counter*delta), ModalModel_myActor_I.OUTPUT.Mode_Y[0]);
    }
    else if(ModalModel_cstate == ModalModel_Mode2) {
      fprintf(f1, "%g %g\n", (counter*delta), ModalModel_myActor2_I.OUTPUT.Mode2_Y[0]);
    }
    ModalModel_cstate = ModalModel(ModalModel_cstate);
    ++counter;
  }
  fflush(f1);
  fclose(f1);
  FILE *f = popen("gnuplot -persistent", "w");
  fprintf(f, "plot '%s' using 1:2 with points title 'Output'\n", F);
  pclose(f);
  return 0;
}




