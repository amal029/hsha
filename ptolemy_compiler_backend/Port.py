import warnings


class Port(object):
    def __init__(self, kwargs):
        self.name = None
        self.t = 'double'
        self.dim = 1
        self.value = None

        try:
            self.name = kwargs['name']
        except Exception:
            raise RuntimeError('Name of input port not provided')

        try:
            self.t = kwargs['t']
        except Exception:
            warnings.warn('Type of input port '+self.name+' assumed double')

        try:
            self.dim = kwargs['dim']
        except Exception:
            warnings.warn('Dimension of input port '+self.name+' assumed 1')

    def codeGen(self):
        raise RuntimeError('Port subclass should implement the codeGenration')

    @staticmethod
    def parse(node, **kwargs):
        value = list(node.iterfind('.//property[@name="defaultValue"]'))
        kw = {'name': node.get('name'),
              'value': value[0].get('value') if value != [] else '',
              'dim': kwargs['dim'],
              't': 'double'}
        if kwargs['isInput']:
            return InputPort(kw)
        else:
            return OutputPort(kw)


class InputPort(Port):
    def __init__(self, kwargs):
        super().__init__(kwargs)

    def codeGen(self):
        ret = (self.t + ' ' + self.name + ' [' + str(self.dim) + ']' + ';\n')
        ret += ('unsigned char '+self.name + '_event[1];')
        return ret


class OutputPort(Port):
    def __init__(self, kwargs):
        super().__init__(kwargs)

    def codeGen(self):
        ret = (self.t + ' ' + self.name + ' [' + str(self.dim) + ']' + ';\n')
        ret += ('unsigned char '+self.name + '_event['+str(self.dim)+'];')
        return ret
