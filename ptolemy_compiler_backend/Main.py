#!/usr/bin/env python3

from xml.etree import ElementTree as ET
import sys
import Entity
import re
import warnings

# TODO: Handle continous merge on outputs only
# TODO: Handle connections between modalmodels


def getInputs(links, expressions, modelName):
    expression_values = {e.get('name'):
                         e.find('./property[@name="expression"]').get('value')
                         for e in expressions}
    p = re.compile('\d+(\.\d+)?')
    ret = dict()
    for l in links:
        iname = l.get('port').split('.')
        if iname[0].strip() == modelName:
            value = [ll.get('port') for ll in links
                     if ll.get('relation') == l.get('relation')
                     and ll != l]
            if iname[1].strip() not in ret.keys():
                ret[iname[1]] = value
            else:
                ret[iname[1]] += value

    for k in ret:
        ret[k] = [expression_values[v.split('.')[0].strip()]
                  if v.split('.')[0].strip() in expression_values
                  and p.match(expression_values[v.split('.')[0].strip()])
                  is not None
                  else None
                  for v in ret[k]]
    ret = {k: [v for v in ret[k] if v is not None]
           for k in ret}
    ret = {k: ret[k]
           for k in ret
           if ret[k] != []}

    return ret


def getOthers(i, links):
    k = [i.split('.')[0], 'Expression']
    il = [l for l in links
          if l.get('port') == i]
    if il != []:
        il = il[0]
        ols = [ll for ll in links
               if ll != il]
        return [l.get('port')
                for l in ols
                if l.get('relation') == il.get('relation')
                and l.get('port').split('.')[0] not in k]
    else:
        return il


def getInput(mn, od, sn):
    ret = None
    for k in od:
        if k == 'o':
            continue
        elif k == 'i':
            if sn in [o.get('name')
                      for o in od[k]]:
                ret = mn+'_I.INPUT.'+sn
        else:
            oss = [o.name for o in od[k][0].inputs]
            if sn in oss:
                ret = mn+'_'+k+'.INPUT.'+sn
    return ret


def getOutput(mn, od, sn):
    ret = None
    for k in od:
        if k == 'i':
            continue
        elif k == 'o':
            if sn in [o.get('name')
                      for o in od[k]]:
                ret = mn+'_I.OUTPUT.'+sn
        else:
            oss = [o.name for o in od[k][0].outputs]
            if sn in oss:
                ret = mn+'_'+k+'.OUTPUT.'+sn
    return ret


# FIXME: One should or the events for all the channels of the port
# instead of just checking the 0th event being set. And we should also
# reset the whole event array to 0.
def build_outputs(model_struct, overall, root_links,
                  temp_args):
    for k in model_struct:
        # First we will traverse the outputs
        for k1 in model_struct[k]:
            if k1 == 'o':
                # Surely the outputs
                for i in model_struct[k][k1]:
                    # Get all the inputs connected to this output
                    o = getOthers(k+'.'+i.get('name'), root_links)
                    for oo in o:
                        on = oo.split('.')[0]
                        onn = oo.split('.')[1]
                        if on not in model_struct.keys():
                            if on in ACTORS:
                                # FIXME: This assumes a single channel
                                # type being output.
                                overall.append(
                                    'if ('+k+'_I.OUTPUT.'+i.get('name') +
                                    '_event[0]) {' +
                                    on+'_I.'+onn +
                                    '('+k+'_I.OUTPUT.'+i.get('name')+');\n' +
                                    k+'_I.OUTPUT.'+i.get('name') +
                                    '_event[0] = 0;\n'
                                    '}')
                            else:
                                warnings.warn("Don't know how to connect: "
                                              + k+'.'+i.get('name') + ' to '
                                              + oo)
                        else:
                            # XXX: Assumption that user defined ports
                            # are single channel only.
                            oss = getInput(on, model_struct[on], onn)
                            if oss is not None:
                                overall.append('if ('+k+'_I.OUTPUT.' +
                                               i.get('name')+'_event[0]) {')
                                overall.append(on+'_I.INPUT.'+onn +
                                               '[0] = '+k+'_I.OUTPUT.' +
                                               i.get('name') + '[0];')
                                overall.append(k+'_I.OUTPUT.'+i.get('name') +
                                               '_event[0] = 0;')
                                overall.append('}')
            elif k1 != 'i':
                # There can be outputs here
                for i in model_struct[k][k1][0].outputs:
                    o = getOthers(k+'.'+i.name, root_links)
                    for oo in o:
                        on = oo.split('.')[0]
                        onn = oo.split('.')[1]
                        if on not in model_struct.keys():
                            if on in ACTORS:
                                temp_args[on][0] -= 1
                                overall.append('unsigned char cond'+i.name +
                                               ' = ')
                                overall.append('||'.join(
                                    [k+'_' + model_struct[k][k1][0].name +
                                     '_I.OUTPUT.' + i.name +
                                     '_event['+str(ii)+']'
                                     for ii in range(i.dim)]
                                ))
                                overall.append(';')
                                overall.append('if(cond'+i.name+') {')
                                overall.append(
                                    on + '_I.'+onn+'(' + str(i.dim) + ', ' +
                                    k+'_'+model_struct[k][k1][0].name +
                                    '_I.OUTPUT.' + i.name
                                    + ', '+str(temp_args[on][0])+');')
                                overall.append('memset('+k+'_' +
                                               model_struct[k][k1][0].name +
                                               '_I.OUTPUT.' + i.name +
                                               '_event, 0, '+str(i.dim)+');')
                                overall.append('}')
                            else:
                                warnings.warn("Don't know how to connect: "
                                              + k+'.'+i.name + ' to '
                                              + oo)
                        else:
                            oss = getInput(on, model_struct[on], onn)
                            if oss is not None:
                                overall.append('unsigned char cond'+i.name +
                                               ' = ')
                                overall.append('||'.join(
                                    [k+'_' + model_struct[k][k1][0].name +
                                     '_I.OUTPUT.' + i.name +
                                     '_event['+str(ii)+']'
                                     for ii in range(i.dim)]
                                ))
                                overall.append(';')
                                overall.append('if(cond'+i.name+') {')

                                overall.append(
                                    oss + ' = ' +
                                    k+'_'+model_struct[k][k1][0].name +
                                    '_I.OUTPUT.' + i.name + ';')

                                overall.append('memset('+k+'_' +
                                               model_struct[k][k1][0].name +
                                               '_I.OUTPUT.' + i.name +
                                               '_event, 0, '+str(i.dim)+');')
                                overall.append('}')


def build_inputs(model_struct, overall, root_links,
                 temp_args):
    for k in model_struct:
        # First we will traverse the inputs
        for k1 in model_struct[k]:
            if k1 == 'i':
                # Surely the inputs
                for i in model_struct[k][k1]:
                    # Get all the outputs connected to this input
                    o = getOthers(k+'.'+i.get('name'), root_links)
                    for oo in o:
                        on = oo.split('.')[0]
                        onn = oo.split('.')[1]
                        if on not in model_struct.keys():
                            if on in ACTORS:
                                # FIXME: Assuming that the user defined
                                # ports are single channel and only
                                # carry type double
                                overall.append('data = ' +
                                               on+'_I.'+onn+'();')
                                overall.append(
                                    k+'_I.INPUT.'+i.get('name') +
                                    '[0] = (data == nullptr) ? ' +
                                    k+'_I.INPUT.'+i.get('name') + '[0] : ' +
                                    'data[0];')
                            else:
                                warnings.warn("Don't know how to connect: "
                                              + k+'.'+i.get('name') + ' to '
                                              + oo)

                        else:
                            oss = getInput(on, model_struct[on], onn)
                            if oss is not None:
                                overall.append(
                                    k+'_I.INPUT.'+i.get('name') +
                                    '[0] = '+oss+'[0];')

            elif k1 != 'o':
                # There can be inputs here
                for i in model_struct[k][k1][0].inputs:
                    o = getOthers(k+'.'+i.name, root_links)
                    for oo in o:
                        on = oo.split('.')[0]
                        onn = oo.split('.')[1]
                        if on not in model_struct.keys():
                            if on in ACTORS:
                                overall.append(k+'_I.INPUT.'+i.name +
                                               ' = '+on+'_I.'+onn+'();')
                            else:
                                warnings.warn("Don't know how to connect: "
                                              + k+'.'+i.name + ' to '
                                              + oo)

                        else:
                            oss = getOutput(on, model_struct[on], onn)
                            if oss is not None:
                                overall.append(k + '_' +
                                               model_struct[k][k1][0].name +
                                               '_I.INPUT.' + i.name + ' = '
                                               + oss +
                                               ';')


def build_actor_connections(ACTORS, root_links, temp_args, overall):
    def getOther(root_links, ACTORS, rel):
        return [l
                for l in root_links
                if l.get('relation') == rel
                and l.get('port').split('.')[0] in ACTORS]

    temp_args = dict(temp_args)
    for l in root_links:
        p = l.get('port').split('.')
        if p[0] in ACTORS and p[1] == 'output':
            # Now get its connection to some other ACTOR
            # First remove this actor from ACTORS list
            OACTORS = [a
                       for a in ACTORS
                       if a != p[0]]
            others = getOther(root_links, OACTORS,
                              l.get('relation'))
            for o in others:
                o = o.get('port').split('.')
                temp_args[o[0]][0] -= 1
                overall.append('dim = '+p[0]+'_I.getDim();')
                overall.append(o[0]+'_I.'+o[1]+'(' +
                               # The dimension of the input channel
                               'dim, ' +
                               p[0]+'_I.output(), ' +
                               str(temp_args[o[0]][0]) +
                               ');')


def main(fileName, ACTORS, ACTOR_HEADER):
    # The header file
    root = ET.parse(fileName)

    # TODO: First get the expressions.
    # XXX: Only expressions can feed into the modal models.
    root_links = root.findall('link')

    model_struct = dict()

    temp_args = {a: [0, 0] for a in ACTORS}
    # Now add the registered actors into the model_struct
    for l in root_links:
        p = l.get('port').split('.')
        if p[0] in ACTORS:
            if p[1] == 'input':
                temp_args[p[0]][0] += 1
            else:
                temp_args[p[0]][1] += 1

    # First get all the modal models declared in the example
    for model in root.iterfind(".//*[@class='"
                               "ptolemy.domains.modal.modal.ModalModel']"):
        ret = getInputs(root_links,
                        root.findall('entity[@class="ptolemy.actor.'
                                     'lib.Expression"]'),
                        model.get('name'))
        # Build the function for this modal model
        hs, bs, modelio, modeios = Entity.ModalModel.parse(model, ret, root)
        model_struct[model.get('name')] = ({'i':
                                            modelio[model.get('name')][0],
                                            'o':
                                            modelio[model.get('name')][1]})
        model_struct[model.get('name')].update(modeios[model.get('name')])
        with open(model.get('name')+'.hpp', 'wt') as f:
            f.write('#ifndef '+model.get('name').upper()+'_H\n')
            f.write('#define '+model.get('name').upper()+'_H\n')
            f.write('#include<stdio.h>\n')
            f.write('#include<stdlib.h>\n')
            f.write('extern double delta;\n')
            f.writelines(hs)
            f.write('\n#endif')
        with open(model.get('name')+'.cpp', 'wt') as f:
            f.write('#include "'+model.get('name')+'.hpp"\n\n')
            f.writelines(bs)

    # Now make the the overall buffer
    overall = []
    oheaders = []
    oheaders.append('#include<strings.h>')
    oheaders.append('#include<string.h>')
    for k in model_struct:
        oheaders.append('#include "'+k+'.hpp"')
    overall.append('#include "'+ACTOR_HEADER+'/actors.hpp"')
    # Declare the actor instances here.
    for k in temp_args:
        overall.append(k+'<double,'+str(temp_args[k][0])+',' +
                       str(temp_args[k][1])+'>'+k+'_I;')
    oheaders.append('void connections(void);')
    overall.append('void connections(void) {')
    overall.append('size_t dim;')
    overall.append('//We are assuming here that all library functions '
                   'return double pointers')
    overall.append('double* data;')
    build_outputs(model_struct, overall, root_links, temp_args)
    build_actor_connections(ACTORS, root_links, temp_args, overall)
    build_inputs(model_struct, overall, root_links, temp_args)
    # Now add the actors to actors connection if any
    overall.append('}')
    head = '\n'.join(oheaders)
    with open(fileName.split('.')[0]+'.hpp', 'wt') as o:
        o.writelines(head)
    body = '#include"'+fileName.split('.')[0]+'.hpp"\n'
    body += '\n'.join(overall)
    with open(fileName.split('.')[0]+'.cpp', 'wt') as o:
        o.writelines(body)


if __name__ == '__main__':
    import os
    # main('model.xml')
    # main('waterTanks2_sastry_onlyInit.xml')
    ACTOR_LIB = os.environ[('ACTOR_LIB')]+'/'
    ACTOR_HEADER_DIR = ACTOR_LIB+'h'
    # Now parse the registry to find the actors that are defined.
    ACTORS = list()
    with open(ACTOR_LIB+'registry', 'rt') as f:
        ACTORS = f.readlines()
    ACTORS = [a.strip() for a in ACTORS]
    main(sys.argv[1], ACTORS, ACTOR_HEADER_DIR)
