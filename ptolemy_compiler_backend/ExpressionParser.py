from lark import Lark, Transformer

# XXX: Note indexing ensures that you can either give a single index or
# only two numbers in the index. Also we know that in a LTI system all
# states are row vectors, so the first sum in indexing has to be zero!
struct_grammar = """
    ?sum: product
        | sum "+" product   -> add
        | sum "-" product   -> sub
    ?product: atom
        | product "*" atom  -> mul
        | product "/" atom  -> div
        | product "^" atom  -> exp
    ?atom: NUMBER           -> number
         | "-" atom         -> neg
         | "(" sum ")"
         | struct
    ?struct: var
            | index
            | struct "." struct   -> struct
    ?index: var "(" indexing ")"  -> index
    ?indexing: NUMBER
             | NUMBER "," NUMBER  -> indexing
    ?var: NAME              -> var
    %import common.CNAME    -> NAME
    %import common.NUMBER
    %import common.WS_INLINE
    %ignore WS_INLINE
"""

action_grammar = """
    ?starta: struct "=" sum            -> action
          | struct "=" "[" vsindex "]" -> baction
          | starta ";" starta      -> actions
    ?vsindex: sum
            | vsindex "," sum -> vsindexing
""" + struct_grammar

guard_grammar = """
    ?startg: logic
           | startg "&&" startg -> andd
           | startg "||" startg -> orr
           | "(" startg ")"

    ?logic: struct ">" sum     -> gt
           |struct "<" sum     -> lt
           |struct ">=" sum    -> geq
           |struct "<=" sum    -> leq
           |struct "!=" sum    -> neq
           |struct "==" sum    -> eq
""" + struct_grammar


class AGP(Transformer):
    def __init__(self, vars, mvars, event):
        self.vars = vars
        self.mvars = mvars
        self.event = event

    def div(self, input):
        return ('('+str(input[0])+' / '+str(input[1])+')')

    def mul(self, input):
        return ('('+str(input[0])+' * '+str(input[1])+')')

    def sub(self, input):
        return ('('+str(input[0])+' - '+str(input[1])+')')

    def add(self, input):
        return ('('+str(input[0])+' + '+str(input[1])+')')

    def number(self, n):
        return str(n[0])

    def neg(self, n):
        n = n[0]
        return ('(-'+str(n)+')')

    # XXX: RESTRICTION all user defined ports can only be single
    # dimensional of type double.
    def modalCheck(self, n):
        mname = list(self.mvars.keys())[0]
        ins = self.mvars[mname][0]
        outs = self.mvars[mname][1]

        # Is it in the user defined input of modal model?
        if not self.event:
            return (mname+'_I.INPUT.'+n+'[0]' if n in [i.get('name')
                                                       for i in ins]
                    else mname+'_I.OUTPUT.'+n+'[0]' if n in [o.get('name')
                                                             for o in outs]
                    else None)
        else:
            # Now you should give back the event type
            return (mname+'_I.OUTPUT.'+n+'_event[0]'
                    if n in [o.get('name')
                             for o in outs]
                    else None)

    def var(self, n):
        mname = list(self.mvars.keys())[0]
        ret = None
        # This is giving the initial state vector
        if n[0] == 'I' or n[0] == 'initialStates':
            return '_IS'
        elif n[0] == 'LTI':
            return None
        # This is the actor instance
        elif n[0] in [self.vars[k][0].name
                      for k in self.vars]:
            return mname+'_'+n[0]+'_I'

        # This is for I/O signals
        p = n[0].split('_')
        pi = None
        if p[-1].isdigit():
            pi = p[-1]
            del(p[-1])
            n[0] = '_'.join(p)
        for k in self.vars:
            refs = self.vars[k]
            for r in refs:
                rname = r.name
                rins = [i.name for i in r.inputs]
                routs = [i.name for i in r.outputs]
                # Event parse
                if n[0] in routs and self.event:
                    ret = (mname+'_'+rname+'_I.OUTPUT.'+n[0]+'_event')
                elif n[0] in rins:
                    # Now get the input ports dim
                    # dim = [i.dim for i in r.inputs
                    #        if i.name == n[0]][0]
                    ret = ((mname+'_'+rname+'_I.INPUT.'+n[0]) +
                           (  # '[0]' if dim == 1 else
                               ('['+pi+']' if pi is not None else '')))
                elif n[0] in routs:
                    # Now get the output ports dim
                    # dim = [i.dim for i in r.outputs
                    #        if i.name == n[0]][0]
                    ret = ((mname+'_'+rname+'_I.OUTPUT.'+n[0]) +
                           (  # '[0]' if dim == 1 else
                            ('['+pi+']' if pi is not None else '')))

        # Now check if this exists in the modal model
        ret = self.modalCheck(n[0]) if ret is None else ret

        return ret if ret is not None else str(n[0])


class StructTransformer(AGP):
    def __init__(self, vars, mvars, event):
        super().__init__(vars, mvars, event)

    def sindex(self, input):
        return input[0]+'.'+input[1]

    def index(self, input):
        return input[0]+'['+input[1]+']'

    def indexing(self, input):
        if int(input[0]) != 0:
            raise RuntimeError('All vectors are single row vectors, found'
                               ' not single row vector:' + input[0])
        return input[1]

    def struct(self, input):
        ret = ''
        if input[0] is not None:
            ret += input[0]+'.'
        if input[1] is not None:
            return ret+input[1]


class ActionTransformer(StructTransformer):
    def __init__(self, vars, mvars, event):
        super().__init__(vars, mvars, event)

    def actions(self, action):
        return '\n'.join(action)

    def baction(self, name):
        if isinstance(name[1], list):
            ret = ';\n'.join([name[0]+'['+str(i)+'] = ' + v
                             for i, v in enumerate(name[1])])
        else:
            ret = name[0] + '[0] = ' + name[1] + ';\n'
        return ret

    def vsindexing(self, input):
        return [input[0], input[1]]

    def action(self, name):
        # This is the only place where event would matter
        # TODO: CHECK (potential bug)
        if not self.event:
            return str(name[0]) + ' = ' + str(name[1]) + ';'
        else:
            return str(name[0]) + ' = 1;'


class GuardTransformer(StructTransformer):

    def __init__(self, vars, mvars, event):
        super().__init__(vars, mvars, event)

    def orr(self, actions):
        return ('('+str(actions[0])+' || '+actions[1]+')')

    def andd(self, actions):
        return ('('+str(actions[0])+' && '+str(actions[1])+')')

    def gt(self, action):
        return ('('+str(action[0])+' > '+str(action[1])+')')

    def lt(self, action):
        return ('('+str(action[0])+' < '+str(action[1])+')')

    def geq(self, action):
        return ('('+str(action[0])+' >= '+str(action[1])+')')

    def leq(self, action):
        return ('('+str(action[0])+' <= '+str(action[1])+')')

    def eq(self, action):
        return ('('+str(action[0])+' == '+str(action[1])+')')

    def neq(self, action):
        return ('('+str(action[0])+' != '+str(action[1])+')')


class Parser:

    def __init__(self, vars=None, model_vars=None, event=None):
        self.at = ActionTransformer(vars, model_vars, event)
        self.gt = GuardTransformer(vars, model_vars, event)
        self.st = StructTransformer(vars, model_vars, event)

        self.actions = Lark(action_grammar, parser='lalr',
                            start='starta', transformer=self.at).parse
        self.structs = Lark(struct_grammar, parser='lalr',
                            start='struct', transformer=self.st).parse
        self.guards = Lark(guard_grammar, parser='lalr',
                           start='startg',
                           transformer=self.gt).parse

    def input_parse(self, arg):
        # Try to parse the input string one after another.
        ret = None
        try:
            ret = self.actions(arg)
        except Exception as e1:
            try:
                ret = self.guards(arg)
            except Exception as e2:
                print('\n\n\n', e1)
                print('\n\n\n', e2)
                raise RuntimeError('Cannot parse string: ', arg, '\n')
        return ret


if __name__ == '__main__':
    parser = Parser()
    print('\n')
    # Example of actions
    ret = parser.input_parse('a1 = 4/8; a = b')
    print(ret)
    # Example of guards
    ret = parser.input_parse('a == 4/8')
    print(ret)
    # Example of struct guards
    ret = parser.input_parse('Mode.I(0,7) > 7+a')
    print(ret)
    # Example of struct set actions
    ret = parser.input_parse('Mode2.I = [Mode_state(1)]')
    print(ret)
    ret = parser.input_parse('Mode2.I = [Mode_state(1), Mode.I(0)]')
    print(ret)
