/* This file defines all the Ptolemy actor structures that can be used
   with HIOA */
/* Author: Avinash Malik */
/* Date: 16/Mar/2018 */


#ifndef ACTORS_H
#define ACTORS_H

// The continousmerge actor
// Takes "N" inputs and produces one output.
#include<array>
#include<assert.h>
#include <iostream>

template<class T, size_t INPUT_SIZE, size_t OUTPUT_SIZE>
class ContinuousMerge
{
private:
  struct channel_data {
    size_t size;
    T* data;
    channel_data (size_t nc, T* data) :
      size(nc), data(data) {}
  };
  std::array<channel_data*, INPUT_SIZE> a;
  // This is the input function
public:
  ContinuousMerge(){
    a.fill(nullptr);
  }
  void input(size_t nc, T* v, size_t index){
    assert(index < INPUT_SIZE);
    this->a[index] = new channel_data(nc, v);
  }

  // This is the output function
  T* output() {
    T* ret = nullptr;
    for (size_t i = 0; i < INPUT_SIZE; ++i)
      if (a[i] != nullptr){
	ret = (ret == nullptr) ? a[i]->data : ret;
	delete a[i];
	a[i] = nullptr;
      }
    return ret;
  }

  // This gives back the dimensions of the channel_data
  size_t getDim() {
    size_t ret = 0;
    for (size_t i = 0; i < INPUT_SIZE; ++i)
      if (a[i] != nullptr){
	ret = a[i]->size;
	break;
      }
    return ret;
  }
};

// This is the time delay actor 
#include<queue>
extern double time_delay;  	// The time-delay defined by user
extern double delta; 		// This delta step-size
extern size_t k;		// This is the step-counter
template<class T, size_t INPUT_SIZE, size_t OUTPUT_SIZE>
class TimeDelay
{
public:
  T* output() {
    T* ret = nullptr;
    if (k*delta >= time_delay) {
      ret = this->q.front();
      this->q.pop();
      return ret;
    }
    return nullptr;
  }

  size_t getDim(){
    if (k*delta > time_delay) {
      size_t ret = this->s.front();
      this->s.pop();
      return ret;
    }
  }

  void input(size_t nc, T* v, size_t index){
    assert(index < INPUT_SIZE);
    this->q.push(v);
    this->s.push(nc);
  }
private:
  std::queue<T*> q;
  std::queue<size_t> s;
};

#endif
